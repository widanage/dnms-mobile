package com.cwidanage.dnms.services;

import com.cwidanage.dnms.DNMSApp;
import com.cwidanage.dnms.services.tasks.InitialSyncTask;
import com.cwidanage.dnms.util.ResponseTracker;
import com.cwidanage.dnms.util.logging.LogManager;
import com.cwidanage.dnms.util.logging.Logger;

import java.io.SyncFailedException;


/**
 * //todo check for detached events and delete them
 *
 * @author Chathura Widanage
 */

public class SyncService {

    private final static Logger logger = LogManager.getLogger(SyncService.class);

    public final static SyncService INSTANCE = new SyncService();

    private SyncService() {

    }

    public void doUpSync(final SyncListener syncListener) {
        final ResponseTracker responseTracker = new ResponseTracker(4);
        responseTracker.setOnDoneListener(new ResponseTracker.OnDoneListener() {
            @Override
            public void onDone(int total, int failed) {
                if (failed == 0) {
                    syncListener.onDone("Synchronized with server successfully");
                } else {
                    syncListener.onError(new SyncFailedException("Syncing with server failed"));
                }
            }
        });
        doCodesFetch(new SyncListener() {
            @Override
            public void onDone(String msg) {
                responseTracker.responseReceived();
                syncListener.onProgress(msg);
            }

            @Override
            public void onProgress(String msg) {
                syncListener.onProgress(msg);
            }

            @Override
            public void onError(Exception e) {
                syncListener.onError(e);
            }
        });

        doTeiUpSync(new SyncListener() {
            @Override
            public void onDone(String msg) {
                responseTracker.responseReceived();
                syncListener.onProgress(msg);
                doEnrollmentSync(new SyncListener() {
                    @Override
                    public void onDone(String msg) {
                        syncListener.onProgress(msg);
                        responseTracker.responseReceived();
                    }

                    @Override
                    public void onProgress(String msg) {
                        syncListener.onProgress(msg);
                    }

                    @Override
                    public void onError(Exception e) {
                        syncListener.onError(e);
                        responseTracker.requestFailed();
                    }
                });

                doEventsUpSync(new SyncListener() {
                    @Override
                    public void onDone(String msg) {
                        syncListener.onProgress(msg);
                        responseTracker.responseReceived();
                    }

                    @Override
                    public void onProgress(String msg) {
                        syncListener.onProgress(msg);
                    }

                    @Override
                    public void onError(Exception e) {
                        syncListener.onError(e);
                        responseTracker.requestFailed();
                    }
                });
            }

            @Override
            public void onProgress(String msg) {
                syncListener.onProgress(msg);
            }

            @Override
            public void onError(Exception e) {
                syncListener.onError(e);
            }
        });
    }

    public void doCodesFetch(final SyncListener syncListener) {
        CodesService.INSTANCE.syncFromServer(syncListener);
    }

    public void doEventsUpSync(SyncListener syncListener) {
        logger.debug("Starting event syncing");
        EventService.INSTANCE.syncToServer(syncListener);
    }

    public void doEnrollmentSync(final SyncListener syncListener) {
        logger.debug("Starting enrollment syncing");
        EnrollmentService.INSTANCE.syncToServer(syncListener);
    }

    public void doTeiUpSync(final SyncListener syncListener) {
        logger.debug("Starting TEI up syncing");
        TrackedEntityInstanceService.INSTANCE.syncToServer(syncListener);
    }

    public void doInitSync(final SyncListener syncListener) {
        logger.debug("Starting initial synchronization");

        InitialSyncTask initialSyncTask = new InitialSyncTask(syncListener);
        initialSyncTask.executeOnExecutor(DNMSApp.EXECUTOR);
    }
}
