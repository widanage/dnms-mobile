package com.cwidanage.dnms.constants;

/**
 * @author Chathura Widanage
 */

public enum TrackedEntityAttribute {

    BIRTH_WEIGHT("atlI71TBcR3", "Birth Weight"),
    CHDR_NUMBER("WqdldQpOIxm", "CHDR Number"),
    DOB("AtK3fDqU8uu", "Date of Birth"),
    DSD_AREA("IRDGLDH6NVY", "Eligible Family Number"),
    ELIGIBILE_FAMILY_NUMBER("lqkUJD8Em4E", "Eligible Family Number"),
    ETHNICITY("V79VT8TNxwN", "Ethnicity"),
    FIRST_NAME("izuwkaOUgFg", "First Name"),
    FIXED_TELEPHONE_NUMBER("XetR60AsyIP", "Fixed Telephone Number"),
    GENDER("BZEpuufLyDE", "Gender"),
    HOME_ADDRESS("yrwhNAkOnnb", "Home Address"),
    LAST_NAME("C8DBAo2wEYN", "Last Name"),
    MOH_AREA("kctg21ROun4", "MOH Area"),
    NAME_OF_GUARDIAN("l62g9SZ1e8A", "Name of Guardian/Parent"),
    NIC("XtSbvKcoTyD", "NIC"),
    NUMBER_OF_FAMIMLIES_IN_THE_HOUSE("V6WZXsDSt3K", "Number of families in the house"),
    NUMBER_OF_PERSONS_IN_THE_HOUSE("QeFh4BvabYw", "Number of persons in the house"),
    NUMBER_OF_PERSONS_IN_RISK_IN_HOUSE("clioVMwW18G", "Number of persons with at risk for malnutrition in family"),
    OTHER_NAMES("cEgZWZzT9QM", "Other Names"),
    PHI_AREA("roYXvHnXKOI", "PHI Area"),
    PHM_AREA("grQLfhqqJ8F", "PHM Area"),
    PHN("wZ6V9Mi5wnf", "PHN"),
    RELIGION("ISgZd6IBxCX", "Religion"),
    TP_OF_PARENT("eSEJ7xPrRi7", "Telephone Number of Parent"),
    THRIPOSHA_STATUS("GU1IVU9xwbT", "Thriposha Status"),
    VILLEGE_NAME("FNOnxgwznRc", "Village Name");

    private String id;
    private String displayName;

    TrackedEntityAttribute(String id, String displayName) {
        this.id = id;
        this.displayName = displayName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
