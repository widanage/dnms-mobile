package com.cwidanage.dnms.entity;

import com.cwidanage.dnms.R;
import com.cwidanage.dnms.constants.EnrollmentStatus;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.exceptions.validation.ValidationFailedException;
import com.cwidanage.dnms.services.CodesService;
import com.cwidanage.dnms.services.InstanceSpecificIDs;
import com.cwidanage.dnms.services.OrganizationUnitService;
import com.cwidanage.dnms.util.JsonIgnore;
import com.orm.dsl.Ignore;
import com.orm.dsl.Unique;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Chathura Widanage
 */

public class Enrollment extends AbstractEntity {

    private Date lastUpdated;

    private String orgUnit;

    @Ignore
    private String program = InstanceSpecificIDs.PROGRAM;

    @Ignore
    @JsonIgnore
    private OrganizationUnit organizationUnit;

    @Unique
    private String enrollment;

    private String trackedEntityInstance;

    private boolean followup;

    private Date enrollmentDate;

    private Date incidentDate;

    private EnrollmentStatus status = EnrollmentStatus.ACTIVE;

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public Date getIncidentDate() {
        return incidentDate;
    }

    public void setIncidentDate(Date incidentDate) {
        this.incidentDate = incidentDate;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(String orgUnit) {
        this.orgUnit = orgUnit;
    }

    public OrganizationUnit getOrganizationUnit() {
        return organizationUnit;
    }

    public void setOrganizationUnit(OrganizationUnit organizationUnit) {
        this.organizationUnit = organizationUnit;
    }

    public String getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(String enrollment) {
        this.enrollment = enrollment;
    }

    public String getTrackedEntityInstance() {
        return trackedEntityInstance;
    }

    public void setTrackedEntityInstance(String trackedEntityInstance) {
        this.trackedEntityInstance = trackedEntityInstance;
    }

    public boolean isFollowup() {
        return followup;
    }

    public void setFollowup(boolean followup) {
        this.followup = followup;
    }

    public Date getEnrollmentDate() {
        return enrollmentDate;
    }

    public void setEnrollmentDate(Date enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
    }

    public EnrollmentStatus getStatus() {
        return status;
    }

    public void setStatus(EnrollmentStatus status) {
        this.status = status;
    }

    @Override
    public void loadRelations() throws NotFoundInDBException {
        this.organizationUnit = OrganizationUnitService.INSTANCE.getById(this.orgUnit);
    }

    @Override
    public void saveToDb(boolean noValidation) throws ValidationFailedException {
        if (enrollment == null) {
            enrollment = CodesService.INSTANCE.burrowCode();
        }

        if (!noValidation) {
            if (orgUnit == null) {
                throw new ValidationFailedException(R.string.null_org_unit_enrollment);
            }

            if (trackedEntityInstance == null) {
                throw new ValidationFailedException(R.string.null_tei_enrollment);
            }
        }

        if (enrollmentDate == null) {
            enrollmentDate = Calendar.getInstance().getTime();
        }

        if (incidentDate == null) {
            incidentDate = Calendar.getInstance().getTime();
        }

        Enrollment.update(this);
    }
}
