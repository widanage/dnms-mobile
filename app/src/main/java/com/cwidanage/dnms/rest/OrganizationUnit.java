package com.cwidanage.dnms.rest;

/**
 * @author Chathura Widanage
 */

public class OrganizationUnit extends AbstractRestModel {

    private String id;
    private String displayName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public com.cwidanage.dnms.entity.OrganizationUnit getEntity() {
        com.cwidanage.dnms.entity.OrganizationUnit ou = new com.cwidanage.dnms.entity.OrganizationUnit();
        ou.setOuId(this.id);
        ou.setDisplayName(this.displayName);
        return ou;
    }
}
