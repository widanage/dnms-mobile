package com.cwidanage.dnms.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cwidanage.dnms.R;
import com.cwidanage.dnms.constants.TrackedEntityAttribute;
import com.cwidanage.dnms.entity.Enrollment;
import com.cwidanage.dnms.entity.OrganizationUnit;
import com.cwidanage.dnms.entity.TrackedEntityInstance;
import com.cwidanage.dnms.util.DateUtils;
import com.cwidanage.dnms.util.SyncStatus;

import java.util.Date;
import java.util.List;

/**
 * @author Chathura Widanage
 */

public class SyncStatusAdapter extends RecyclerView.Adapter<SyncStatusAdapter.SyncStatusViewHolder> {

    private List<SyncStatus> syncStatuses;

    public SyncStatusAdapter(List<SyncStatus> syncStatuses) {
        this.syncStatuses = syncStatuses;
    }

    @Override
    public SyncStatusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sync_status_card, parent, false);
        SyncStatusViewHolder syncStatusViewHolder = new SyncStatusViewHolder(itemView);
        return syncStatusViewHolder;
    }

    @Override
    public void onBindViewHolder(SyncStatusViewHolder holder, int position) {
        SyncStatus syncStatus = this.syncStatuses.get(position);

        holder.msg.setText(syncStatus.getMsg());

        switch (syncStatus.getType()) {
            case ERROR:
                holder.icon.setImageResource(R.drawable.canceled);
                break;
            case PROGRESS:
                holder.icon.setImageResource(R.drawable.tick);
        }
    }

    @Override
    public int getItemCount() {
        return this.syncStatuses.size();
    }

    public class SyncStatusViewHolder extends RecyclerView.ViewHolder {

        private TextView msg;
        private ImageView icon;

        public SyncStatusViewHolder(View itemView) {
            super(itemView);
            msg = (TextView) itemView.findViewById(R.id.sync_status_txt);
            icon = (ImageView) itemView.findViewById(R.id.sync_status_icon);
        }
    }
}
