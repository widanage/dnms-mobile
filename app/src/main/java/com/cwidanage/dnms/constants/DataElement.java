package com.cwidanage.dnms.constants;

import com.cwidanage.dnms.R;

/**
 * @author Chathura Widanage
 */

public enum DataElement {

    AGE_OF_CHILD_IN_MONTHS("GDhOcklahIq", R.string.age_of_child_in_months, null, R.drawable.age_ico),
    CHILD_LOCATION_COORDINATE("D9GRKMT4Czq", R.string.child_location_coordinate, null),
    HEIGHT("Bpovp931fOZ", R.string.height, null, R.drawable.height),
    HEIGHT_FOR_AGE_CATEGORY("bYTh3TBpAFF", R.string.height_for_age_category, null),
    WEIGHT("jRceYOPJeCO", R.string.weight, null, R.drawable.weight),
    WEIGHT_FOR_AGE_CATEGORY("qh8ptEnFWmp", R.string.weight_for_age_category, null),
    WEIGHT_FOR_HEIGHT_CATEGORY("RGmYXRckjv0", R.string.weight_for_height_category, null),

    R1_POVERTY_OR_POOR_INCOME_MANAGEMENT("BTwe8lvhr4b", R.string.r1_poverty_or_poor_income_management, null),
    R1_IIILIMITED_OPPORTUNITY_FOR_INCOME_GENERATION_WITHIN_AGRICULTURE_SETTINGS("AHRXmsMNbIu",
            R.string.r1_iiilimited_opportunity_for_income_generation_within_agriculture_settings,
            R1_POVERTY_OR_POOR_INCOME_MANAGEMENT),
    R1_II_POOR_FINANCIAL_MANAGEMENT("cmK5179Zq0d", R.string.r1_ii_poor_financial_management, R1_POVERTY_OR_POOR_INCOME_MANAGEMENT),
    R1_I_LOW_INCOME("SG5bELDIJMt", R.string.r1_i_low_income, R1_POVERTY_OR_POOR_INCOME_MANAGEMENT),
    R1_IV_ANY_CONSTRAINS_IN_OBTAINING_LOAN("JRCCfrYDoQM", R.string.r1_iv_any_constrains_in_obtaining_loan, R1_POVERTY_OR_POOR_INCOME_MANAGEMENT),
    R1_V_FEWER_OPPORTUNITIES_FOR_OCCUPATIONAL_TRAININGS("mOjDNEJdCPF", R.string.r1_v_fewer_opportunities_for_occupational_trainings, R1_POVERTY_OR_POOR_INCOME_MANAGEMENT),

    R2_INADEQUATE_CHILD_CARE("FXW7KOfmfV3", R.string.r2_inadequate_child_care, null),
    R2_I_BOTH_PARENTS_ARE_WORKING_AND_DO_NOT_HAVE_TIME_TO_SPENT_WITH_CHILDREN("wzmgGGAZM6j", R.string.r2_i_both_parents_are_working_and_do_not_have_time_to_spent_with_children, R2_INADEQUATE_CHILD_CARE),
    R2_III_POOR_KNOWLEDGE_ON_ECCD_AMONG_SERVICE_PROVIDERS("Ajc3gRLHGk0", R.string.r2_iii_poor_knowledge_on_eccd_among_service_providers, R2_INADEQUATE_CHILD_CARE),
    R2_II_POOR_KNOWLEDGE_ON_ECCD_AMONG_CARE_GIVERS("S1lA6BjQk92", R.string.r2_ii_poor_knowledge_on_eccd_among_care_givers, R2_INADEQUATE_CHILD_CARE),
    R2_IV_UNAVAILABILITY_OF_MINIMUM_PLAY_MATERIALS("WdqQ4jyUAp8", R.string.r2_iv_unavailability_of_minimum_play_materials, R2_INADEQUATE_CHILD_CARE),
    R2_V_ABSENCE_OF_AGE_APPROPRIATE_IMMUNIZATION("M9VsyFW0FgJ", R.string.r2_v_absence_of_age_appropriate_immunization, R2_INADEQUATE_CHILD_CARE),

    R3_POOR_CHILD_FEEDING_PRACTICES("eQj4eOiKezp", R.string.r3_poor_child_feeding_practices, null),
    R3_III_INADEQUATE_FREQUENCY_OF_FEEDING("fvKRRFvlOYf", R.string.r3_iii_inadequate_frequency_of_feeding, R3_POOR_CHILD_FEEDING_PRACTICES),
    R3_I_INADEQUATE_QUANTITY_PER_MEALS("ZFcx2jmd6dc", R.string.r3_i_inadequate_quantity_per_meals, R3_POOR_CHILD_FEEDING_PRACTICES),
    R3_II_POOR_QUALITY_OF_THE_MEALS("ukgrWYgOTx5", R.string.r3_ii_poor_quality_of_the_meals, R3_POOR_CHILD_FEEDING_PRACTICES),
    R3_IV_POOR_KNOWLEDGE_ON_OBTAINING_NUTRITIOUS_FOOD_WITHIN_THE_AVAILABLE_RESOURCES("Kmtx2Q8YeXj", R.string.r3_iv_poor_knowledge_on_obtaining_nutritious_food_within_the_available_resources, R3_POOR_CHILD_FEEDING_PRACTICES),
    R3_V_FALSE_BELIEVES_AND_MYTHS("fvAkZTDHkej", R.string.r3_v_false_believes_and_myths, R3_POOR_CHILD_FEEDING_PRACTICES),
    R3_VII_POOR_KNOWLEDGE_AND_ATTITUDES_TOWARDS_NUTRITION("UEqkoYxSDZL", R.string.r3_vii_poor_knowledge_and_attitudes_towards_nutrition, R3_POOR_CHILD_FEEDING_PRACTICES),
    R3_VI_POOR_KNOWLEDGE_OF_FEEDING_DURING_ILLNESS("X4OHuFSgp7k", R.string.r3_vi_poor_knowledge_of_feeding_during_illness, R3_POOR_CHILD_FEEDING_PRACTICES),

    R4_HIGH_PREVALENCE_OF_COMMUNICABLE_DISEASES("wwGpJEEij1j", R.string.r4_high_prevalence_of_communicable_diseases, null),
    R4_III_SUBJECTED_TO_FEVER_FREQUENTLY("xfJrzclMRA0", R.string.r4_iii_subjected_to_fever_frequently, R4_HIGH_PREVALENCE_OF_COMMUNICABLE_DISEASES),
    R4_II_SUBJECTED_TO_DIARRHEAL_DISEASES_FREQUENTLY("GpMuvVmikqg", R.string.r4_ii_subjected_to_diarrheal_diseases_frequently, R4_HIGH_PREVALENCE_OF_COMMUNICABLE_DISEASES),
    R4_I_SUBJECTED_TO_RESPIRATORY_INFECTION_FREQUENTLY("uOGHqGmu1r9", R.string.r4_i_subjected_to_respiratory_infection_frequently, R4_HIGH_PREVALENCE_OF_COMMUNICABLE_DISEASES),

    R5_LOW_FOOD_SECURITY("SQGnjhiUiR5", R.string.r5_low_food_security, null),
    R5_III_INADEQUATE_ALLOCATION_OF_MONEY_FOR_FOODS("SwXTE98GiT2", R.string.r5_iii_inadequate_allocation_of_money_for_foods, R5_LOW_FOOD_SECURITY),
    R5_II_NON_CONSUMPTION_OF_FOODS_FROM_HOME_GARDENING_OR_BACKYARD_FAMING("FF76DMCZaau", R.string.r5_ii_non_consumption_of_foods_from_home_gardening_or_backyard_faming, R5_LOW_FOOD_SECURITY),
    R5_I_NO_HOME_GARDENING("hIdYoGmOnxT", R.string.r5_i_no_home_gardening, R5_LOW_FOOD_SECURITY),
    R5_IV_POOR_HARVEST_FROM_AGRICULTURE_AND_LIVESTOCK("jlSG1VMi5w7", R.string.r5_iv_poor_harvest_from_agriculture_and_livestock, R5_LOW_FOOD_SECURITY),
    R5_IX_DOMESTIC_VIOLENCE_ABUSE_DUE_TO_ALCOHOL_AND_SMOKING("iThNT73AUNV", R.string.r5_ix_domestic_violence_abuse_due_to_alcohol_and_smoking, R5_LOW_FOOD_SECURITY),
    R5_VIII_SPENDING_HIGHER_PROPORTION_OF_INCOME_TO_ALCOHOL("JM11BX22G0V", R.string.r5_viii_spending_higher_proportion_of_income_to_alcohol, R5_LOW_FOOD_SECURITY),
    R5_VI_INADEQUATE_PREVENTIVE_MEASURES_FOR_PROTECTING_HARVEST("gjoFV1B8t2J", R.string.r5_vi_inadequate_preventive_measures_for_protecting_harvest, R5_LOW_FOOD_SECURITY),
    R5_VII_ONE_OR_BOTH_PARENT_CONSUMING_EXCESSIVE_ALCOHOL_OR_SMOKING("I9pT7vgZZ2B", R.string.r5_vii_one_or_both_parent_consuming_excessive_alcohol_or_smoking, R5_LOW_FOOD_SECURITY),
    R5_V_POOR_KNOWLEDGE_AND_ATTITUDES_TOWARDS_FOOD_PREPARATION_AND_PRESERVATION("zw73eoN7nx0", R.string.r5_v_poor_knowledge_and_attitudes_towards_food_preparation_and_preservation, R5_LOW_FOOD_SECURITY),

    R6_INADEQUATE_WATER_AND_SANITATION("ipRAwsU9ZWQ", R.string.r6_inadequate_water_and_sanitation, null),
    R6_II_UNAVAILABILITY_SANITATION("IZ9U9POhBPL", R.string.r6_ii_unavailability_sanitation, R6_INADEQUATE_WATER_AND_SANITATION),
    R6_I_UNAVAILABILITY_SAFE_WATER_SUPPLY("T74FRikzd5j", R.string.r6_i_unavailability_safe_water_supply, R6_INADEQUATE_WATER_AND_SANITATION),

    R7_POOR_NUTRITION_KNOWLEDGE("jY98YpVPefN", R.string.r7_poor_nutrition_knowledge, null),
    R7_III_FALSE_BELIEVES_MYTHS_AND_CUSTOMS("jDnvVHoZHEt", R.string.r7_iii_false_believes_myths_and_customs, R7_POOR_NUTRITION_KNOWLEDGE),
    R7_II_POOR_KNOWLEDGE_ON_USING_APPROPRIATE_FOOD_FOR_FAMILY_MEMBERS("nDEWSHmLiEl", R.string.r7_ii_poor_knowledge_on_using_appropriate_food_for_family_members, R7_POOR_NUTRITION_KNOWLEDGE),
    R7_I_POOR_KNOWLEDGE_AND_ATTITUDES_TOWARDS_NUTRITION("tWCk3b5WbAH", R.string.r7_i_poor_knowledge_and_attitudes_towards_nutrition, R7_POOR_NUTRITION_KNOWLEDGE),

    R8_OTHER("NSbpbPARIFB", R.string.r8_other, null),

    REASON_ANEMIA("kerTmzDMqUB", R.string.reason_anemia, null),
    REASON_LOW_BIRTH_WEIGHT("Sf3lPKs8oLs", R.string.reason_low_birth_weight, null),
    REASON_OVERWEIGHT("l0WWFNEMoQZ", R.string.reason_overweight, null),
    REASON_UNDER_FIVE_WITH_STUNTING("hUxgdSCMiNy", R.string.reason_under_five_with_stunting, null),
    REASON_UNDER_FIVE_WITH_UNDERWEIGHT("gaZGnRTcR7T", R.string.reason_under_five_with_underweight, null),
    REASON_UNDER_FIVE_WITH_WASTING("oTLBvqfHCxz", R.string.reason_under_five_with_wasting, null),

    RP1_POVERTY_OR_POOR_INCOME_MANAGEMENT("S5XP897BURM", R.string.rp1_poverty_or_poor_income_management, null),
    RP2_INADEQUATE_CHILD_CARE("YE4uVLeU8EM", R.string.rp2_inadequate_child_care, null),
    RP3_POOR_CHILD_FEEDING_PRACTICES("Hb14uc3H9ko", R.string.rp3_poor_child_feeding_practices, null),
    RP4_HIGH_PREVALENCE_OF_COMMUNICABLE_DISEASES("E0sVhYx39LL", R.string.rp4_high_prevalence_of_communicable_diseases, null),
    RP5_LOW_FOOD_SECURITY("onGiInVMtSI", R.string.rp5_low_food_security, null),
    RP6_INADEQUATE_WATER_AND_SANITATION("upcFEsa73yC", R.string.rp6_inadequate_water_and_sanitation, null),
    RP7_POOR_NUTRITION_KNOWLEDGE("UTznqW62qjS", R.string.rp7_poor_nutrition_knowledge, null),
    RP8_OTHER("bbIHlKtBECH", R.string.rp8_other, null),

    TEST1("QbKiUgE739f", R.string.test1, null);

    private int stringId;
    private String id;
    private DataElement parent;
    private int iconId;

    DataElement(String id, int stringId, DataElement parent, int iconId) {
        this.id = id;
        this.stringId = stringId;
        this.parent = parent;
        this.iconId = iconId;
    }

    DataElement(String id, int stringId, DataElement parent) {
        this(id, stringId, parent, -1);
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public int getStringId() {
        return stringId;
    }

    public void setStringId(int stringId) {
        this.stringId = stringId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DataElement getParent() {
        return parent;
    }

    public void setParent(DataElement parent) {
        this.parent = parent;
    }
}
