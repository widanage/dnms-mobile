package com.cwidanage.dnms.services;

import com.cwidanage.dnms.entity.Setting;

import java.util.List;

/**
 * @author Chathura Widanage
 */

public class SettingService {

    public final static SettingService INSTANCE = new SettingService();

    private SettingService() {
    }

    public Setting get(String key) {
        List<Setting> settings = Setting.find(Setting.class, "KEY = ?", key);
        if (!settings.isEmpty()) {
            return settings.get(0);
        }
        return null;
    }

    public void put(String key, String value) {
        Setting setting = new Setting();
        setting.setKey(key);
        setting.setValue(value);
        Setting.update(setting);
    }

    public void remove(String key) {
        Setting.deleteAll(Setting.class, "VALUE = ?", key);
    }
}
