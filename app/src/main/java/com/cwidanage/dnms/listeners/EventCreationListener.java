package com.cwidanage.dnms.listeners;

import com.cwidanage.dnms.entity.Event;

/**
 * @author Chathura Widanage
 */

public interface EventCreationListener {
    void onEventCreated(Event event, boolean newEvent, int eventPosition);
}
