package com.cwidanage.dnms;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

import com.cwidanage.dnms.adapters.SelectableItemHolder;
import com.cwidanage.dnms.constants.DataElement;
import com.cwidanage.dnms.constants.RiskIdentificationDataElements;
import com.cwidanage.dnms.entity.DataValue;
import com.cwidanage.dnms.entity.Event;
import com.cwidanage.dnms.exceptions.validation.ValidationFailedException;
import com.cwidanage.dnms.listeners.EventCreationListener;
import com.cwidanage.dnms.services.CodesService;
import com.cwidanage.dnms.util.DateUtils;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


/**
 * @author Chathura Widanage
 */
public class RiskIdentificationEventFragment extends DialogFragment {

    private static final String ARG_TEI_ID = "teiId";

    private String teiId;

    private EventCreationListener mListener;


    public RiskIdentificationEventFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    public static RiskIdentificationEventFragment newInstance(String teiId, EventCreationListener listener) {
        RiskIdentificationEventFragment fragment = new RiskIdentificationEventFragment();
        fragment.mListener = listener;
        Bundle args = new Bundle();
        args.putString(ARG_TEI_ID, teiId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            teiId = getArguments().getString(ARG_TEI_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_risk_identification_event, container, false);
        final Event event = new Event();
        List<DataValue> dataValueList = new ArrayList<>();
        event.setDataValues(dataValueList);
        event.setFresh(true);
        event.setShouldSync(true);
        event.setEvent(CodesService.INSTANCE.burrowCode());
        event.setTrackedEntityInstance(this.teiId);
        event.setProgramStage(com.cwidanage.dnms.constants.Event.RISK_IDENTIFICATION.getId());

        Button cancelButton = (Button) view.findViewById(R.id.cancel_btn);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Button saveButton = (Button) view.findViewById(R.id.save_btn);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    event.saveToDb();
                    Toast.makeText(getContext(), R.string.success_saving_event, Toast.LENGTH_LONG).show();
                    mListener.onEventCreated(event, true, 0);
                    dismiss();
                } catch (ValidationFailedException e) {
                    Toast.makeText(getContext(), R.string.error_saving_event, Toast.LENGTH_LONG).show();
                }
            }
        });

        final EditText dateTxt = (EditText) view.findViewById(R.id.event_date_txt);
        dateTxt.setText(DateUtils.formatFromStandardDate(Calendar.getInstance().getTime()));
        event.setEventDate(Calendar.getInstance().getTime());
        dateTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                try {
                    Date parsedDate = DateUtils.parseToStandardDate(dateTxt.getText().toString());
                    calendar.setTime(parsedDate);
                } catch (ParseException e) {
                    Log.e("TEI_PROFILE", "Error in parsing date", e);
                    //ignore
                }
                new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String dateString = DateUtils.formatFromStandardDate(calendar.getTime());
                        dateTxt.setText(dateString);
                        event.setEventDate(calendar.getTime());
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        //Options
        ScrollView optionsList = (ScrollView) view.findViewById(R.id.options_list);
        TreeNode root = TreeNode.root();
        Iterator<DataElement> mainRisksIterator = RiskIdentificationDataElements.MAP.keySet().iterator();
        while (mainRisksIterator.hasNext()) {
            DataElement mainRisk = mainRisksIterator.next();
            TreeNode parent = new TreeNode(getContext().getString(mainRisk.getStringId()));

            DataValue mainRiskDataValue = new DataValue();
            mainRiskDataValue.setDataElement(mainRisk.getId());
            dataValueList.add(mainRiskDataValue);

            parent.setViewHolder(new SelectableItemHolder(getActivity(), mainRiskDataValue));
            parent.setSelectable(false);
            for (DataElement subRisk : RiskIdentificationDataElements.MAP.get(mainRisk)) {
                DataValue subRiskDataValue = new DataValue();
                subRiskDataValue.setDataElement(subRisk.getId());
                dataValueList.add(subRiskDataValue);

                TreeNode child0 = new TreeNode(getContext().getString(subRisk.getStringId()));
                child0.setViewHolder(new SelectableItemHolder(getActivity(), subRiskDataValue));
                parent.addChild(child0);
            }
            root.addChild(parent);
        }
        AndroidTreeView tView = new AndroidTreeView(getActivity(), root);
        tView.setDefaultAnimation(true);

        optionsList.addView(tView.getView());
        return view;
    }


    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
*/
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
