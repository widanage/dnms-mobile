package com.cwidanage.dnms.services;

/**
 * @author Chathura Widanage
 */

public class SyncListenerAdapater implements SyncListener {

    @Override
    public void onDone(String msg) {

    }

    @Override
    public void onProgress(String msg) {

    }

    @Override
    public void onError(Exception e) {

    }
}
