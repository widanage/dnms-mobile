package com.cwidanage.dnms.util;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Chathura Widanage
 */

public class ResponseTracker {

    private AtomicInteger finishedCount = new AtomicInteger(0);
    private final int expectedResponses;
    private AtomicInteger failures = new AtomicInteger(0);

    private OnDoneListener onDoneListener;

    public void setOnDoneListener(OnDoneListener onDoneListener) {
        this.onDoneListener = onDoneListener;
    }

    public ResponseTracker(int expectedResponses) {
        this.expectedResponses = expectedResponses;
    }

    public boolean requestFailed() {
        this.failures.incrementAndGet();
        this.finishedCount.incrementAndGet();
        return this.isDone();
    }

    public int getFailures() {
        return failures.get();
    }

    public boolean responseReceived() {
        this.finishedCount.incrementAndGet();
        return this.isDone();
    }

    public boolean isDone() {
        boolean done = this.finishedCount.get() == this.expectedResponses;
        if (done && this.onDoneListener != null) {
            this.onDoneListener.onDone(expectedResponses, getFailures());
        }
        return done;
    }

    public interface OnDoneListener {
        void onDone(int total, int failed);
    }
}
