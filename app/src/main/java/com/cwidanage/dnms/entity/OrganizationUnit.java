package com.cwidanage.dnms.entity;

import com.orm.dsl.Table;
import com.orm.dsl.Unique;

/**
 * @author Chathura Widange
 */

@Table
public class OrganizationUnit extends AbstractEntity {

    @Unique
    private String ouId;
    private String displayName;

    public String getOuId() {
        return ouId;
    }

    public void setOuId(String ouId) {
        this.ouId = ouId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public void saveToDb(boolean noValidation) {
        OrganizationUnit.update(this);
    }
}
