package com.cwidanage.dnms.exceptions;

/**
 * @author Chathura Widanage
 */

public class NotFoundInDBException extends Exception {

    public NotFoundInDBException(String msg) {
        super(msg);
    }
}
