package com.cwidanage.dnms;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cwidanage.dnms.constants.DataElement;
import com.cwidanage.dnms.constants.TrackedEntityAttribute;
import com.cwidanage.dnms.entity.DataValue;
import com.cwidanage.dnms.entity.Event;
import com.cwidanage.dnms.entity.TrackedEntityInstance;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.services.InstanceSpecificIDs;
import com.cwidanage.dnms.services.SDCategoryService;
import com.cwidanage.dnms.services.TrackedEntityInstanceService;
import com.cwidanage.dnms.util.DateUtils;
import com.cwidanage.dnms.util.logging.LogManager;
import com.cwidanage.dnms.util.logging.Logger;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * @author Chathura Widanage
 */
public class AnayticsFragment extends Fragment {

    private final static Logger logger = LogManager.getLogger(AnayticsFragment.class);

    private static final String TEI_ID = "TEI_ID";

    private String teiId;
    private TrackedEntityInstance trackedEntityInstance;

    private LineChart weightAgeChart;
    private LineChart heightAgeChart;

    private OnFragmentInteractionListener mListener;

    public AnayticsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment AnayticsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AnayticsFragment newInstance(String param1) {
        AnayticsFragment fragment = new AnayticsFragment();
        Bundle args = new Bundle();
        args.putString(TEI_ID, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            teiId = getArguments().getString(TEI_ID);
            if (teiId != null) {
                try {
                    this.trackedEntityInstance = TrackedEntityInstanceService.INSTANCE.getById(this.teiId);
                    this.trackedEntityInstance.loadRelations();
                } catch (NotFoundInDBException e) {
                    e.printStackTrace();
                }
            } else {//new

            }
        }
    }

    private void initChart(final LineChart chart, float[][] data, float readings[]) {
        chart.setPinchZoom(true);
        chart.setDrawGridBackground(true);
        chart.setGridBackgroundColor(Color.parseColor("#c413e0"));

        int colors[] = {0xef1010, 0xebbc18, 0xc8eaa2, 0x1de826};

        LineData lineData = new LineData();
        for (int i = 0; i < 4; i++) {
            LineDataSet line1 = new LineDataSet(new ArrayList<Entry>(), "Line" + i);
            line1.setAxisDependency(YAxis.AxisDependency.LEFT);
            line1.setColor(colors[3 - i]);
            line1.setCircleColor(colors[3 - i]);
            line1.setDrawCircles(false);
            line1.setLineWidth(2f);
            line1.setCircleRadius(3f);
            line1.setFillAlpha(255);
            line1.setDrawFilled(true);
            line1.setFillColor(colors[3 - i]);
            line1.setHighLightColor(Color.rgb(244, 117, 117));
            line1.setDrawCircleHole(false);
            line1.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return chart.getAxisLeft().getAxisMinimum();
                }
            });
            lineData.addDataSet(line1);
        }


        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < 4; j++) {
                lineData.addEntry(new Entry(i, data[i][j]), 3 - j);
            }
        }

        //plotting actual data
        LineDataSet plot = new LineDataSet(new ArrayList<Entry>(), "Plot");
        plot.setColor(Color.BLACK);
        plot.setCircleColor(Color.BLACK);
        plot.setCircleRadius(3f);
        plot.setDrawCircleHole(false);
        for (int i = 0; i < readings.length; i++) {
            if (readings[i] != 0) {
                plot.addEntry(new Entry(i, readings[i]));
            }
        }
        lineData.addDataSet(plot);


        chart.setData(lineData);
        chart.invalidate();

        chart.getDescription().setText("");
        chart.getLegend().setEnabled(false);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_anaytics, container, false);

        float[] heights = new float[60];
        float[] weights = new float[60];


        String childDobString = this.trackedEntityInstance.getOrDefaultAttributeValue(TrackedEntityAttribute.DOB.getId(), null);
        if (childDobString == null) {
            //todo handle
        } else {
            try {
                Date dobObject = DateUtils.parseToStandardDate(childDobString);
                List<Event> events = this.trackedEntityInstance.getEvents();
                for (Event event : events) {
                    Date eventDate = event.getEventDate();
                    int month = Math.round(DateUtils.getDateDifferenceInMonths(eventDate, dobObject));
                    if (month >= 0 && month < 60) {
                        if (event.getProgramStage().equals(InstanceSpecificIDs.PROGRAM_STAGE_NUT_MONITORING)) {
                            List<DataValue> dataValues = event.getDataValues();
                            for (DataValue dataValue : dataValues) {
                                if (dataValue.getDataElement().equals(DataElement.HEIGHT.getId())) {
                                    heights[month] = Float.parseFloat(dataValue.getValue());
                                } else if (dataValue.getDataElement().equals(DataElement.WEIGHT.getId())) {
                                    weights[month] = Float.parseFloat(dataValue.getValue());
                                }
                            }
                        }
                    }
                }

                logger.debug(Arrays.toString(heights));
                logger.debug(Arrays.toString(weights));


                this.weightAgeChart = (LineChart) view.findViewById(R.id.weight_age);
                initChart(this.weightAgeChart, SDCategoryService.getWeightData(false), weights);

                this.heightAgeChart = (LineChart) view.findViewById(R.id.height_age);
                initChart(this.heightAgeChart, SDCategoryService.getHeightData(false), heights);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
