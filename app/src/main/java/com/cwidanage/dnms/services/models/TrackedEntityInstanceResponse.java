package com.cwidanage.dnms.services.models;

import com.cwidanage.dnms.entity.TrackedEntityInstance;

import java.util.List;

/**
 * @author Chathura Widanage
 */

public class TrackedEntityInstanceResponse {

    private List<TrackedEntityInstance> trackedEntityInstances;

    private Pager pager;

    public Pager getPager() {
        return pager;
    }

    public void setPager(Pager pager) {
        this.pager = pager;
    }

    public List<TrackedEntityInstance> getTrackedEntityInstances() {
        return trackedEntityInstances;
    }

    public void setTrackedEntityInstances(List<TrackedEntityInstance> trackedEntityInstances) {
        this.trackedEntityInstances = trackedEntityInstances;
    }

    @Override
    public String toString() {
        return trackedEntityInstances.toString();
    }
}
