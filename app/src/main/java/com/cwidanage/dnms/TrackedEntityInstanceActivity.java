package com.cwidanage.dnms;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cwidanage.dnms.adapters.ViewPagerAdapter;
import com.cwidanage.dnms.constants.Event;
import com.cwidanage.dnms.constants.Gender;
import com.cwidanage.dnms.entity.Coordinate;
import com.cwidanage.dnms.entity.OrganizationUnit;
import com.cwidanage.dnms.entity.TrackedEntityInstance;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.exceptions.validation.ValidationFailedException;
import com.cwidanage.dnms.listeners.EventCreationListener;
import com.cwidanage.dnms.services.EventService;
import com.cwidanage.dnms.services.TrackedEntityInstanceService;
import com.cwidanage.dnms.util.logging.LogManager;
import com.cwidanage.dnms.util.logging.Logger;

import java.util.Locale;

public class TrackedEntityInstanceActivity extends AppCompatActivity
        implements ProfileFragment.OnFragmentInteractionListener,
        EventsFragment.OnFragmentInteractionListener, AnayticsFragment.OnFragmentInteractionListener{

    private final static Logger logger = LogManager.getLogger(TrackedEntityInstanceActivity.class);

    private TrackedEntityInstance trackedEntityInstance;


    //--- Header fields ---//
    private TextView teiNameHdrTxt;
    private TextView teiAgeHdrTxt;
    private TextView teiOuHdrTxt;
    private ImageView profileImage;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private String teiId;

    private boolean hasUnsavedChanges;

    private ViewPagerAdapter viewPagerAdapter;

    private EventsFragment eventsFragment;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teiprofile);

        this.tabLayout = (TabLayout) findViewById(R.id.tabs);
        this.viewPager = (ViewPager) findViewById(R.id.viewpager);
        this.tabLayout.setupWithViewPager(this.viewPager);

        this.teiNameHdrTxt = (TextView) findViewById(R.id.teiNameHdrTxt);
        this.teiAgeHdrTxt = (TextView) findViewById(R.id.teiAgeHdrTxt);
        this.teiOuHdrTxt = (TextView) findViewById(R.id.teiOuHdrTxt);
        this.profileImage = (ImageView) findViewById(R.id.profile_image);

        /*Other fields*/
        //this.teiCHDRTxt = (EditText) findViewById(R.id.teiCHDRTxt);

        this.teiId = getIntent().getStringExtra("TEI_ID");

        try {
            this.trackedEntityInstance = TrackedEntityInstanceService.INSTANCE.getById(teiId);
        } catch (NotFoundInDBException e) {
            logger.error("TEI with id %s not found in DB", e, teiId);
        }

        updateFields();
        setupViewPager();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        /*Binding Fields*/
        //this.bindEditTextWithTeiAttribute(this.teiCHDRTxt, TrackedEntityAttribute.CHDR_NUMBER);
    }

    private void setupViewPager() {

        this.eventsFragment = EventsFragment.newInstance(this.teiId);//to use in registration dialog

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(ProfileFragment.newInstance(this.teiId), getString(R.string.profile));
        viewPagerAdapter.addFragment(this.eventsFragment, getString(R.string.events));
        viewPagerAdapter.addFragment(AnayticsFragment.newInstance(this.teiId), getString(R.string.analytics));
        this.viewPager.setAdapter(viewPagerAdapter);
    }

    private void updateFields() {
        String fullName = this.trackedEntityInstance.getFullName();
        String ageString = Math.round(this.trackedEntityInstance.getAge()) + "";
        OrganizationUnit organizationUnit = this.trackedEntityInstance.getOrganizationUnit();
        Locale current = getResources().getConfiguration().locale;
        if (current.getLanguage().equals("si") || current.getLanguage().equals("ta")) {
            ageString = getString(R.string.years) + " " + ageString;
        } else {
            ageString = ageString + " " + getString(R.string.years);
        }

        if (!fullName.trim().isEmpty()) {
            this.teiNameHdrTxt.setText(fullName);
        } else {
            this.teiNameHdrTxt.setText(R.string.unnamed_child);
        }

        this.teiAgeHdrTxt.setText(ageString);
        if (organizationUnit != null) {
            this.teiOuHdrTxt.setText(organizationUnit.getDisplayName());
        } else {
            this.teiOuHdrTxt.setText(R.string.unknown_location);
        }

        String gender = this.trackedEntityInstance.getGender();
        if (gender.equals(Gender.FEMALE.getValue())) {
            this.profileImage.setImageResource(R.drawable.girl_ico);
        } else if (gender.equals(Gender.MALE.getValue())) {
            this.profileImage.setImageResource(R.drawable.boy_ico);
        } else {
            this.profileImage.setImageResource(R.drawable.baby);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tei_profile_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (this.hasUnsavedChanges) {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle(getString(R.string.unsaved_changes));
            alertDialog.setMessage(getString(R.string.unsaved_changes_message));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.stay_in_profile), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.discard_changes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    TrackedEntityInstanceActivity.super.onBackPressed();
                }
            });
            alertDialog.show();
        } else {
            super.onBackPressed();
        }
    }

    private void setTEILocation() {
        final ProgressDialog locationProgress = new ProgressDialog(this);
        locationProgress.setMessage(getString(R.string.fetching_location));
        locationProgress.show();
        try {
            final com.cwidanage.dnms.entity.Event registrationEvent = EventService.INSTANCE.getRegistrationEvent(trackedEntityInstance.getTrackedEntityInstance());

            locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Log.d("Location Fetched", location.toString());
                    double lat = location.getLatitude();
                    double lon = location.getLongitude();

                    Coordinate coordinate = new Coordinate();
                    coordinate.setLatitude(lat);
                    coordinate.setLongitude(lon);
                    coordinate.setEventId(registrationEvent.getEvent());
                    coordinate.setShouldSync(true);

                    registrationEvent.setCoordinate(coordinate);
                    registrationEvent.setShouldSync(true);
                    registrationEvent.saveToDb(false);

                    if (locationProgress.isShowing()) {
                        locationProgress.dismiss();
                    }
                    Toast.makeText(getApplicationContext(), R.string.location_updated, Toast.LENGTH_LONG).show();
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            }, null);
        } catch (SecurityException e) {
            locationProgress.dismiss();
            Toast.makeText(this, R.string.no_location_permission, Toast.LENGTH_SHORT).show();
        } catch (NotFoundInDBException e) {
            locationProgress.dismiss();
            Toast.makeText(this, R.string.no_registration_warning, Toast.LENGTH_SHORT).show();
        }
    }

    private void saveTEI() {
        try {
            this.trackedEntityInstance.saveToDb();
            this.hasUnsavedChanges = false;
            Toast.makeText(this, getString(R.string.changes_saved_msg), Toast.LENGTH_LONG).show();
        } catch (ValidationFailedException e) {
            logger.error("Error in saving TEI", e);
            Toast.makeText(this, "Error occurred when saving : "
                    + getString(e.getStringId()), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save: {
                if (this.trackedEntityInstance.getOrgUnit() == null) {
                    Toast.makeText(getApplicationContext(), R.string.null_org_unit_tei, Toast.LENGTH_LONG).show();
                    return false;
                }
                this.trackedEntityInstance.loadEvents();
                boolean foundRegistration = false;
                for (com.cwidanage.dnms.entity.Event event : this.trackedEntityInstance.getEvents()) {
                    if (event.getProgramStage().equals(Event.REGISTRATION.getId())) {
                        foundRegistration = true;
                    }

                    //set ou for events if null
                    if (event.getOrgUnit() == null) {
                        event.setOrgUnit(this.trackedEntityInstance.getOrgUnit());
                        event.saveToDb(true);
                    }
                }

                if (!foundRegistration) {
                    AlertDialog alertDialog = new AlertDialog.
                            Builder(this)
                            .setMessage(R.string.no_registration_prompt)
                            .setPositiveButton(R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    RegistrationEventFragment registrationEventFragment = RegistrationEventFragment.newInstance(teiId, eventsFragment);
                                    registrationEventFragment.show(getSupportFragmentManager(), "reg-dialog");
                                }
                            })
                            .setNegativeButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    saveTEI();
                                }
                            })
                            .create();
                    alertDialog.show();
                } else {
                    saveTEI();
                }
                return true;
            }
            case android.R.id.home:
                this.onBackPressed();
                return true;
            case R.id.action_update_location: {
                this.setTEILocation();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onHeaderFieldUpdated() {
        this.updateFields();
    }

    @Override
    public void onPropertyChanged() {
        this.hasUnsavedChanges = true;
    }

    @Override
    public void onCreateNewEvent(Event event) {

    }
}

