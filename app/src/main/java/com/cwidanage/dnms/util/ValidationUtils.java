package com.cwidanage.dnms.util;

/**
 * @author Chathura Widanage
 */

public class ValidationUtils {

    public static boolean isEmptyString(String string) {
        return string == null || string.trim().isEmpty();
    }

    public static boolean isValidNumber(String string) {
        try {
            Double.parseDouble(string);
            return true;
        } catch (NumberFormatException nex) {
            return false;
        }
    }
}
