package com.cwidanage.dnms;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cwidanage.dnms.adapters.TrackedEntityInstanceAdapter;
import com.cwidanage.dnms.entity.Event;
import com.cwidanage.dnms.entity.OrganizationUnit;
import com.cwidanage.dnms.entity.TrackedEntityInstance;
import com.cwidanage.dnms.entity.TrackedEntityInstanceAttribute;
import com.cwidanage.dnms.services.CodesService;
import com.cwidanage.dnms.services.OrganizationUnitService;
import com.cwidanage.dnms.services.SyncListener;
import com.cwidanage.dnms.services.SyncService;
import com.cwidanage.dnms.services.TrackedEntityInstanceService;
import com.cwidanage.dnms.util.TextWatcherAdapater;
import com.facebook.stetho.Stetho;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

public class MainActivity extends AppCompatActivity {

    private EditText searchTxt;
    private FloatingActionButton newTeiBtn;

    private List<TrackedEntityInstance> teiList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TrackedEntityInstanceAdapter teiAdapter;

    private ImageView gnListToggleBtn;
    private LinearLayout gnListView;

    private Set<String> selectedGns;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setElevation(0);

        /*STETHO*/
        Stetho.initializeWithDefaults(this);

        searchTxt = (EditText) findViewById(R.id.searchTxt);
        newTeiBtn = (FloatingActionButton) findViewById(R.id.new_tei_btn);
        gnListView = (LinearLayout) findViewById(R.id.gn_list);

        this.selectedGns = new HashSet<>();
        Iterator<OrganizationUnit> all = OrganizationUnitService.INSTANCE.getAll();
        while (all.hasNext()) {
            final OrganizationUnit next = all.next();
            selectedGns.add(next.getOuId());
            CheckBox checkBox = new CheckBox(this);
            checkBox.setText(next.getDisplayName());
            checkBox.setChecked(true);
            checkBox.setTextColor(getResources().getColor(R.color.white));
            checkBox.setTextSize(18);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        selectedGns.add(next.getOuId());
                    } else {
                        selectedGns.remove(next.getOuId());
                    }
                    performSearch();
                }
            });

            gnListView.addView(checkBox);
        }

        gnListToggleBtn = (ImageView) findViewById(R.id.gn_list_toggle_btn);
        gnListToggleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gnListView.getVisibility() == View.GONE) {
                    gnListView.setVisibility(View.VISIBLE);

                } else {
                    gnListView.setVisibility(View.GONE);
                }
            }
        });

        searchTxt.addTextChangedListener(new TextWatcherAdapater() {
            @Override
            public void afterTextChanged(Editable s) {
                performSearch();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.teiLst);
        teiAdapter = new TrackedEntityInstanceAdapter(teiList, new TrackedEntityInstanceAdapter.TeiSelectionListener() {
            @Override
            public void onTeiSelect(TrackedEntityInstance trackedEntityInstance) {
                startProfileActivity(trackedEntityInstance);
            }
        });

        newTeiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startProfileActivity(null);
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(teiAdapter);

        this.updateTeis();
    }

    private void startProfileActivity(TrackedEntityInstance trackedEntityInstance) {
        Intent i = new Intent(MainActivity.this, TrackedEntityInstanceActivity.class);

        if (trackedEntityInstance == null) {
            trackedEntityInstance = new TrackedEntityInstance();
            trackedEntityInstance.setTrackedEntityInstance(CodesService.INSTANCE.burrowCode());
            trackedEntityInstance.setFresh(true);
            trackedEntityInstance.setShouldSync(true);
            trackedEntityInstance.setAttributes(new ArrayList<TrackedEntityInstanceAttribute>());
            trackedEntityInstance.setTemporary(true);
            TrackedEntityInstance.save(trackedEntityInstance);
        }

        i.putExtra("TEI_ID", trackedEntityInstance.getTrackedEntityInstance());
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sync: {
                Intent intent = new Intent(MainActivity.this, LoadingActivity.class);
                intent.putExtra(LoadingActivity.TYPE_UP_SYNC, true);
                startActivity(intent);
                finish();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        TrackedEntityInstanceService.INSTANCE.clearTemp();
        TrackedEntityInstanceService.INSTANCE.clearLastAccess();
        updateTeis();
    }

    /**
     * Call this from non ui threads
     */
    private void notifyTeiListChanged() {
        synchronized (teiAdapter) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    teiAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    private void performSearch() {
        final String searchKeyword = this.searchTxt.getText().toString();
        DNMSApp.EXECUTOR.submit(new Runnable() {
            @Override
            public void run() {
                synchronized (teiList) {
                    final List<TrackedEntityInstance> filteredTei = TrackedEntityInstanceService.INSTANCE.search(searchKeyword, selectedGns);
                    teiList.clear();
                    teiList.addAll(filteredTei);
                    notifyTeiListChanged();
                }
            }
        });
    }

    private void updateTeis() {
        DNMSApp.EXECUTOR.submit(new Runnable() {
            @Override
            public void run() {
                synchronized (teiList) {
                    final List<TrackedEntityInstance> trackedEntityInstances = TrackedEntityInstance.listAll(TrackedEntityInstance.class);
                    teiList.clear();
                    teiList.addAll(trackedEntityInstances);
                    notifyTeiListChanged();
                }
            }
        });
    }
}
