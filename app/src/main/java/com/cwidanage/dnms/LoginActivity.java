package com.cwidanage.dnms;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.cwidanage.dnms.entity.Setting;
import com.cwidanage.dnms.services.AuthService;
import com.cwidanage.dnms.services.SettingService;
import com.cwidanage.dnms.services.SyncListener;

import java.util.Locale;

public class LoginActivity extends AppCompatActivity {

    private final static String KEY_APP_LANGUAGE = "KEY_APP_LANGUAGE";

    private Button loginButton;
    private RadioGroup radioGroup;

    private EditText usernameTxt;
    private EditText passwordTxt;

    private SettingService settingService = SettingService.INSTANCE;

    static {

        System.setProperty("log4j2.disable.jmx", "true");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.loginButton = (Button) findViewById(R.id.btn_login);
        this.radioGroup = (RadioGroup) findViewById(R.id.language_radio_group);
        this.usernameTxt = (EditText) findViewById(R.id.input_email);
        this.passwordTxt = (EditText) findViewById(R.id.input_password);

        this.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.language_radio_sinhala:
                        setAppLanguage("si");
                        break;
                    case R.id.language_radio_tamil:
                        setAppLanguage("ta");
                        break;
                    default:
                        setAppLanguage("en-uk");
                }
            }
        });


        final AuthService authService = AuthService.INSTANCE;

        this.loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFieldsEditable(false);
                String username = usernameTxt.getText().toString();
                String password = passwordTxt.getText().toString();

                authService.login(username, password, new SyncListener() {
                    @Override
                    public void onDone(String msg) {
                        doneAuth();
                    }

                    @Override
                    public void onProgress(String msg) {

                    }

                    @Override
                    public void onError(Exception e) {
                        setFieldsEditable(true);
                        Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

            }
        });


        if (authService.alreadyAuthenticated()) {
            Setting languageSetting = this.settingService.get(KEY_APP_LANGUAGE);
            if (languageSetting != null) {
                this.setAppLanguage(languageSetting.getValue());
            }
            doneAuth();
        } else {
            this.radioGroup.check(R.id.language_radio_sinhala);
            setAppLanguage("si");
        }
    }


    private void doneAuth() {
        Intent i = new Intent(LoginActivity.this, LoadingActivity.class);
        startActivity(i);
        finish();
    }

    private void setFieldsEditable(boolean bool) {
        this.loginButton.setEnabled(bool);
        this.usernameTxt.setEnabled(bool);
        this.passwordTxt.setEnabled(bool);
    }

    private void setAppLanguage(String lang) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        config.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        this.settingService.put(KEY_APP_LANGUAGE, lang);
    }
}
