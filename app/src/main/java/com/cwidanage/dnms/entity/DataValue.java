package com.cwidanage.dnms.entity;

import com.cwidanage.dnms.util.JsonIgnore;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

/**
 * @author Chathura Widanage
 */
@Table
public class DataValue extends AbstractEntity implements UniqueIdGeneratable {

    @JsonIgnore
    private String eventId;
    private String dataElement;
    private String value;

    @JsonIgnore
    @Unique
    private String uniqueId;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getDataElement() {
        return dataElement;
    }

    public void setDataElement(String dataElement) {
        this.dataElement = dataElement;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    @Override
    public void generateUniqueId() {
        this.uniqueId = this.eventId + "_" + this.dataElement;
    }

    @Override
    public void saveToDb(boolean noValidation) {
        this.generateUniqueId();
        DataValue.update(this);
    }

    @Override
    public void setSynced() {
        super.setSynced();
        DataValue.update(this);
    }
}
