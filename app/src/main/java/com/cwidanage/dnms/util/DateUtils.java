package com.cwidanage.dnms.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Chathura Widanage
 */

public class DateUtils {

    private static final Date BEGINING = new Date(0);

    private static final SimpleDateFormat standardDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat standardDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private static final long YEAR_IN_MILLIS = 31556952000L;
    private static final long MONTH_IN_MILLIS = 2419200000L;

    public static String now() {
        return standardDateTimeFormat.format(Calendar.getInstance().getTime());
    }

    public static String dateTimeStringToDateOnlyString(String dateTime) throws ParseException {
        return formatFromStandardDate(standardDateTimeFormat.parse(dateTime));
    }

    public static Date parseToStandardDate(String date) throws ParseException {
        return standardDateFormat.parse(date);
    }

    public static String formatFromStandardDate(Date date) {
        return standardDateFormat.format(date);
    }

    public static float getDateDifferenceInYears(Date d1, Date d2) {
        return ((float) (d1.getTime() - d2.getTime())) / YEAR_IN_MILLIS;
    }

    public static float getDateDifferenceInMonths(Date d1, Date d2) {
        return ((float) (d1.getTime() - d2.getTime())) / MONTH_IN_MILLIS;
    }

    public static double getAgeInMonths(Date dob) {
        float inYeats = getDateDifferenceInYears(Calendar.getInstance().getTime(), dob);
        return Math.floor(inYeats * 12);
    }

    public static SimpleDateFormat getStandardDateFormat() {
        return standardDateFormat;
    }
}
