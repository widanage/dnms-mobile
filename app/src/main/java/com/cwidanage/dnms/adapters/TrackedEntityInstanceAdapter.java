package com.cwidanage.dnms.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cwidanage.dnms.R;
import com.cwidanage.dnms.constants.TrackedEntityAttribute;
import com.cwidanage.dnms.entity.Coordinate;
import com.cwidanage.dnms.entity.Enrollment;
import com.cwidanage.dnms.entity.Event;
import com.cwidanage.dnms.entity.OrganizationUnit;
import com.cwidanage.dnms.entity.TrackedEntityInstance;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.services.EventService;
import com.cwidanage.dnms.util.DateUtils;
import com.cwidanage.dnms.util.logging.Log;

import java.util.Date;
import java.util.List;

/**
 * @author Chathura Widanage
 */

public class TrackedEntityInstanceAdapter extends RecyclerView.Adapter<TrackedEntityInstanceAdapter.TrackedEntityInstanceViewHolder> {

    private List<TrackedEntityInstance> teiList;
    private TeiSelectionListener teiSelectionListener;

    public TrackedEntityInstanceAdapter(List<TrackedEntityInstance> teiList, TeiSelectionListener teiSelectionListener) {
        this.teiList = teiList;
        this.teiSelectionListener = teiSelectionListener;
    }

    @Override
    public TrackedEntityInstanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tracked_entity_instance_card, parent, false);
        TrackedEntityInstanceViewHolder trackedEntityInstanceViewHolder = new TrackedEntityInstanceViewHolder(itemView);
        itemView.setOnClickListener(trackedEntityInstanceViewHolder);
        return trackedEntityInstanceViewHolder;
    }

    @Override
    public void onBindViewHolder(TrackedEntityInstanceViewHolder holder, int position) {
        TrackedEntityInstance trackedEntityInstance = this.teiList.get(position);

        holder.name.setText(trackedEntityInstance.getFullName());

        OrganizationUnit orgUnit = trackedEntityInstance.getOrganizationUnit();
        holder.teiOrgUnit.setText(orgUnit.getDisplayName());

        String teiGender = trackedEntityInstance.getOrDefaultAttributeValue(TrackedEntityAttribute.GENDER.getId(), "unknown");
        switch (teiGender) {
            case "female":
                holder.teiGenderIcon.setImageResource(R.drawable.girl_ico);
                break;
            case "male":
                holder.teiGenderIcon.setImageResource(R.drawable.boy_ico);
                break;
            default:
                //todo set icon
                holder.teiGenderIcon.setImageResource(R.drawable.girl_ico);
        }

        //age in months
        String dob = trackedEntityInstance.getOrDefaultAttributeValue(TrackedEntityAttribute.DOB.getId(), null);
        try {
            Date date = DateUtils.parseToStandardDate(dob);
            double ageInyears = DateUtils.getAgeInMonths(date);
            holder.ageInMonths.setText(String.format("%.0f", ageInyears));
        } catch (Exception e) {
            holder.ageInMonths.setVisibility(View.GONE);
        }

        //enrollment status
        Enrollment enrollment = trackedEntityInstance.getEnrollment();
        if (enrollment == null) {
            holder.enrollmentIcon.setVisibility(View.GONE);
        } else {
            holder.enrollmentIcon.setVisibility(View.VISIBLE);
            switch (enrollment.getStatus()) {
                case COMPLETED:
                    holder.enrollmentIcon.setImageResource(R.drawable.tick);
                    break;
                case CANCELLED:
                    holder.enrollmentIcon.setImageResource(R.drawable.canceled);
                    break;
                case ACTIVE:
                    holder.enrollmentIcon.setImageResource(R.drawable.enroll_icon);
            }
        }

        //location
        try {
            Event registrationEvent = EventService.INSTANCE.getRegistrationEvent(trackedEntityInstance.getTrackedEntityInstance());
            registrationEvent.loadRelations();
            Coordinate coordinate = registrationEvent.getCoordinate();
            if (coordinate == null) {
                holder.locationIcon.setVisibility(View.GONE);
            } else {
                holder.locationIcon.setVisibility(View.VISIBLE);
            }
        } catch (NotFoundInDBException e) {
            holder.locationIcon.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return this.teiList.size();
    }


    public interface TeiSelectionListener {
        void onTeiSelect(TrackedEntityInstance trackedEntityInstance);
    }

    public class TrackedEntityInstanceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView name;
        private TextView ageInMonths;
        private ImageView teiGenderIcon;
        private TextView teiOrgUnit;
        private ImageView enrollmentIcon;
        private ImageView locationIcon;

        public TrackedEntityInstanceViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.teiNameHdrTxt);
            teiGenderIcon = (ImageView) itemView.findViewById(R.id.teiGenderIcon);
            teiOrgUnit = (TextView) itemView.findViewById(R.id.teiOrgUnit);
            ageInMonths = (TextView) itemView.findViewById(R.id.teiAgeInMonths);
            enrollmentIcon = (ImageView) itemView.findViewById(R.id.enrollment_ico);
            locationIcon = (ImageView) itemView.findViewById(R.id.location_ico);
        }


        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            TrackedEntityInstance trackedEntityInstance = teiList.get(adapterPosition);
            teiSelectionListener.onTeiSelect(trackedEntityInstance);
        }
    }
}
