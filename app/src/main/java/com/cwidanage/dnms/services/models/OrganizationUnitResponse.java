package com.cwidanage.dnms.services.models;

import com.cwidanage.dnms.rest.OrganizationUnit;

import java.util.List;

/**
 * @author Chathura Widanage
 */

public class OrganizationUnitResponse {

    private List<OrganizationUnit> organisationUnits;

    public List<OrganizationUnit> getOrganisationUnits() {
        return organisationUnits;
    }

    public void setOrganisationUnits(List<OrganizationUnit> organisationUnits) {
        this.organisationUnits = organisationUnits;
    }
}
