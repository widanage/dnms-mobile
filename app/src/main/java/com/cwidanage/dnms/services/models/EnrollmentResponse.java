package com.cwidanage.dnms.services.models;

import com.cwidanage.dnms.entity.Enrollment;

import java.util.List;

/**
 * @author Chathura Widanage
 */

public class EnrollmentResponse {

    private List<Enrollment> enrollments;

    public List<Enrollment> getEnrollments() {
        return enrollments;
    }

    public void setEnrollments(List<Enrollment> enrollments) {
        this.enrollments = enrollments;
    }
}
