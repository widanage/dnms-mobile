package com.cwidanage.dnms.util;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by chath on 11/11/2017.
 */

public class TextWatcherAdapater implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
