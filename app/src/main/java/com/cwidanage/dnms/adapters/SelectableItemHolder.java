package com.cwidanage.dnms.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.cwidanage.dnms.R;
import com.cwidanage.dnms.constants.DataElement;
import com.cwidanage.dnms.entity.DataValue;
import com.unnamed.b.atv.model.TreeNode;

/**
 * @author Chathura Widanage
 */

public class SelectableItemHolder extends TreeNode.BaseNodeViewHolder<String> {

    private TextView tvValue;
    private CheckBox nodeSelector;
    private ImageView expandIcon;

    private DataValue dataValue;

    public SelectableItemHolder(Context context, DataValue dataValue) {
        super(context);
        this.dataValue = dataValue;
    }

    @Override
    public View createNodeView(final TreeNode node, String value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_selectable_item, null, false);

        tvValue = (TextView) view.findViewById(R.id.node_value);
        nodeSelector = (CheckBox) view.findViewById(R.id.node_check);
        expandIcon = (ImageView) view.findViewById(R.id.expand_icon);

        tvValue.setText(value);

        if (node.getLevel() == 1 && !node.getChildren().isEmpty()) {
            tvValue.setTypeface(tvValue.getTypeface(), Typeface.BOLD);
            view.setPadding(0, 0, 0, 10);
        } else {
            view.setPadding(30, 0, 0, 10);
        }


        if (node.isSelectable()) {
            final String oldValue = dataValue.getValue();
            expandIcon.setVisibility(View.GONE);
            tvValue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    nodeSelector.setChecked(!nodeSelector.isChecked());
                }
            });
            nodeSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    node.setSelected(isChecked);

                    if (node.getParent() != null && node.getParent().getViewHolder() instanceof SelectableItemHolder) {//notify parent to set as selected if this has a parent
                        ((SelectableItemHolder) node.getParent().getViewHolder()).notifyChildSelection(isChecked);
                    }

                    String newValue = String.valueOf(isChecked);
                    dataValue.setValue(newValue);
                    if (newValue.equals(oldValue)) {
                        dataValue.setShouldSync(false);
                    } else {
                        dataValue.setShouldSync(true);
                    }
                }
            });
            nodeSelector.setChecked(node.isSelected());
        } else {
            nodeSelector.setVisibility(View.GONE);
            node.setClickListener(new TreeNode.TreeNodeClickListener() {
                @Override
                public void onClick(TreeNode node, Object value) {
                    if (node.isExpanded()) {
                        expandIcon.setImageResource(R.drawable.ic_expand_more_black_24dp);
                    } else {
                        expandIcon.setImageResource(R.drawable.ic_expand_less_black_24dp);
                    }
                }
            });
        }
        return view;
    }

    private int childSelections = 0;

    public void notifyChildSelection(boolean selected) {
        if (selected) {
            childSelections++;
        } else {
            childSelections--;
        }

        if (childSelections > 0) {
            this.dataValue.setValue("true");
        } else {
            this.dataValue.setValue("false");
        }
    }


    @Override
    public void toggleSelectionMode(boolean editModeEnabled) {
        nodeSelector.setVisibility(editModeEnabled ? View.VISIBLE : View.GONE);
        nodeSelector.setChecked(mNode.isSelected());
    }
}
