package com.cwidanage.dnms.util.logging;

/**
 * @author Chathura Widanage
 */

public class LogManager {

    public static Logger getLogger(Class clazz) {
        return new Logger(clazz.getName());
    }
}
