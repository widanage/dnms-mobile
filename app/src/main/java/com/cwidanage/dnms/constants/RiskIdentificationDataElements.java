package com.cwidanage.dnms.constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author Chathura Widanage
 */

public class RiskIdentificationDataElements {

    public final static HashMap<DataElement, List<DataElement>> MAP = new LinkedHashMap<>();

    static {
        //R1
        List<DataElement> r1 = new ArrayList<>();
        r1.add(DataElement.R1_I_LOW_INCOME);
        r1.add(DataElement.R1_II_POOR_FINANCIAL_MANAGEMENT);
        r1.add(DataElement.R1_IIILIMITED_OPPORTUNITY_FOR_INCOME_GENERATION_WITHIN_AGRICULTURE_SETTINGS);
        r1.add(DataElement.R1_IV_ANY_CONSTRAINS_IN_OBTAINING_LOAN);
        r1.add(DataElement.R1_V_FEWER_OPPORTUNITIES_FOR_OCCUPATIONAL_TRAININGS);
        MAP.put(DataElement.R1_POVERTY_OR_POOR_INCOME_MANAGEMENT, r1);

        //R2
        List<DataElement> r2 = new ArrayList<>();
        r2.add(DataElement.R2_I_BOTH_PARENTS_ARE_WORKING_AND_DO_NOT_HAVE_TIME_TO_SPENT_WITH_CHILDREN);
        r2.add(DataElement.R2_II_POOR_KNOWLEDGE_ON_ECCD_AMONG_CARE_GIVERS);
        r2.add(DataElement.R2_III_POOR_KNOWLEDGE_ON_ECCD_AMONG_SERVICE_PROVIDERS);
        r2.add(DataElement.R2_IV_UNAVAILABILITY_OF_MINIMUM_PLAY_MATERIALS);
        r2.add(DataElement.R2_V_ABSENCE_OF_AGE_APPROPRIATE_IMMUNIZATION);
        MAP.put(DataElement.R2_INADEQUATE_CHILD_CARE, r2);

        //R3
        List<DataElement> r3 = new ArrayList<>();
        r3.add(DataElement.R3_I_INADEQUATE_QUANTITY_PER_MEALS);
        r3.add(DataElement.R3_II_POOR_QUALITY_OF_THE_MEALS);
        r3.add(DataElement.R3_III_INADEQUATE_FREQUENCY_OF_FEEDING);
        r3.add(DataElement.R3_IV_POOR_KNOWLEDGE_ON_OBTAINING_NUTRITIOUS_FOOD_WITHIN_THE_AVAILABLE_RESOURCES);
        r3.add(DataElement.R3_V_FALSE_BELIEVES_AND_MYTHS);
        r3.add(DataElement.R3_VI_POOR_KNOWLEDGE_OF_FEEDING_DURING_ILLNESS);
        r3.add(DataElement.R3_VII_POOR_KNOWLEDGE_AND_ATTITUDES_TOWARDS_NUTRITION);
        MAP.put(DataElement.R3_POOR_CHILD_FEEDING_PRACTICES, r3);

        //R4
        List<DataElement> r4 = new ArrayList<>();
        r4.add(DataElement.R4_I_SUBJECTED_TO_RESPIRATORY_INFECTION_FREQUENTLY);
        r4.add(DataElement.R4_II_SUBJECTED_TO_DIARRHEAL_DISEASES_FREQUENTLY);
        r4.add(DataElement.R4_III_SUBJECTED_TO_FEVER_FREQUENTLY);
        MAP.put(DataElement.R4_HIGH_PREVALENCE_OF_COMMUNICABLE_DISEASES, r4);

        //R5
        List<DataElement> r5 = new ArrayList<>();
        r5.add(DataElement.R5_I_NO_HOME_GARDENING);
        r5.add(DataElement.R5_II_NON_CONSUMPTION_OF_FOODS_FROM_HOME_GARDENING_OR_BACKYARD_FAMING);
        r5.add(DataElement.R5_III_INADEQUATE_ALLOCATION_OF_MONEY_FOR_FOODS);
        r5.add(DataElement.R5_IV_POOR_HARVEST_FROM_AGRICULTURE_AND_LIVESTOCK);
        r5.add(DataElement.R5_V_POOR_KNOWLEDGE_AND_ATTITUDES_TOWARDS_FOOD_PREPARATION_AND_PRESERVATION);
        r5.add(DataElement.R5_VI_INADEQUATE_PREVENTIVE_MEASURES_FOR_PROTECTING_HARVEST);
        r5.add(DataElement.R5_VII_ONE_OR_BOTH_PARENT_CONSUMING_EXCESSIVE_ALCOHOL_OR_SMOKING);
        r5.add(DataElement.R5_VIII_SPENDING_HIGHER_PROPORTION_OF_INCOME_TO_ALCOHOL);
        r5.add(DataElement.R5_IX_DOMESTIC_VIOLENCE_ABUSE_DUE_TO_ALCOHOL_AND_SMOKING);
        MAP.put(DataElement.R5_LOW_FOOD_SECURITY, r5);

        //R6
        List<DataElement> r6 = new ArrayList<>();
        r6.add(DataElement.R6_I_UNAVAILABILITY_SAFE_WATER_SUPPLY);
        r6.add(DataElement.R6_II_UNAVAILABILITY_SANITATION);
        MAP.put(DataElement.R6_INADEQUATE_WATER_AND_SANITATION, r6);

        //R7
        List<DataElement> r7 = new ArrayList<>();
        r7.add(DataElement.R7_I_POOR_KNOWLEDGE_AND_ATTITUDES_TOWARDS_NUTRITION);
        r7.add(DataElement.R7_II_POOR_KNOWLEDGE_ON_USING_APPROPRIATE_FOOD_FOR_FAMILY_MEMBERS);
        r7.add(DataElement.R7_III_FALSE_BELIEVES_MYTHS_AND_CUSTOMS);
        MAP.put(DataElement.R7_POOR_NUTRITION_KNOWLEDGE, r7);

        //R8
        List<DataElement> r8 = new ArrayList<>();
        r8.add(DataElement.R8_OTHER);
        MAP.put(DataElement.R8_OTHER, r8);
    }

}
