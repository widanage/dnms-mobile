package com.cwidanage.dnms.util.gson;

import com.cwidanage.dnms.util.JsonIgnore;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.orm.SugarRecord;

/**
 * @author Chathura Widanage
 */

public class SerializationExclusionStratergy implements ExclusionStrategy {
    @Override
    public boolean shouldSkipField(FieldAttributes f) {
        if (f.getDeclaringClass().equals(SugarRecord.class)) {
            return true;
        }
        return f.getAnnotation(JsonIgnore.class) != null;
    }

    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }
}
