package com.cwidanage.dnms.constants;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Chathura Widanage
 */

public class RegistrationDataElements {

    public static final Set<DataElement> SET = new HashSet<>();

    static {
        SET.add(DataElement.REASON_ANEMIA);
        SET.add(DataElement.REASON_LOW_BIRTH_WEIGHT);
        SET.add(DataElement.REASON_OVERWEIGHT);
        SET.add(DataElement.REASON_UNDER_FIVE_WITH_STUNTING);
        SET.add(DataElement.REASON_UNDER_FIVE_WITH_UNDERWEIGHT);
        SET.add(DataElement.REASON_UNDER_FIVE_WITH_WASTING);
    }
}
