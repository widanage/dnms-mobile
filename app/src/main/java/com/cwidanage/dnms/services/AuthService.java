package com.cwidanage.dnms.services;

import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cwidanage.dnms.entity.Setting;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * @author Chathura Widanage
 */

public class AuthService extends AbstractService {

    public final static AuthService INSTANCE = new AuthService();

    private SettingService settingService = SettingService.INSTANCE;

    private AuthService() {
    }

    public boolean alreadyAuthenticated() {
        Setting setting = settingService.get(KEY_AUTH);
        return setting != null;
    }

    public void login(String username, String password, final SyncListener syncListener) {
        final String authToken = Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
        this.setAuthHeader(authToken);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, getSyncEndpointUrl(null),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("LOGIN", response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONObject("userCredentials").getJSONArray("userRoles");
                            if (jsonArray.length() > 0 && jsonArray.getJSONObject(0).getString("id").equals(InstanceSpecificIDs.PHM_USER_ROLE)) {
                                settingService.put(KEY_AUTH, authToken);
                                syncListener.onDone(null);
                            } else {
                                syncListener.onError(new Exception("Your user roll is not allowed to log in to this application"));
                            }
                        } catch (JSONException e) {
                            Log.e("LOGIN", e.getMessage());
                            syncListener.onError(new Exception("Unexpected response from the server"));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Exception failureEx = error;
                        if (error instanceof AuthFailureError) {
                            failureEx = new Exception("Failed to authenticate");
                        } else if (error instanceof NetworkError) {
                            failureEx = new Exception("Failed to communicate withs server");
                        }
                        syncListener.onError(failureEx);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        fireRequest(stringRequest);
    }

    @Override
    public void syncFromServer(SyncListener syncListener) {

    }

    @Override
    public String getSyncEndpointQueryString(Map<String, String> params) {
        return "me.json?fields=userCredentials[userRoles]";
    }
}
