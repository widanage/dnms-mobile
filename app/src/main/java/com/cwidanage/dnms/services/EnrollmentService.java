package com.cwidanage.dnms.services;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cwidanage.dnms.constants.EnrollmentStatus;
import com.cwidanage.dnms.entity.Enrollment;
import com.cwidanage.dnms.entity.OrganizationUnit;
import com.cwidanage.dnms.entity.TrackedEntityInstance;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.exceptions.validation.ValidationFailedException;
import com.cwidanage.dnms.services.models.EnrollmentResponse;
import com.cwidanage.dnms.services.requests.JsonObjectPostRequest;
import com.cwidanage.dnms.services.requests.GsonRequest;
import com.cwidanage.dnms.services.requests.VolleyResponseListener;
import com.cwidanage.dnms.util.DateUtils;
import com.cwidanage.dnms.util.ResponseTracker;
import com.cwidanage.dnms.util.gson.SerializationExclusionStratergy;
import com.cwidanage.dnms.util.logging.LogManager;
import com.cwidanage.dnms.util.logging.Logger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.SyncFailedException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Chathura Widanage
 */

public class EnrollmentService extends AbstractService {

    public final static EnrollmentService INSTANCE = new EnrollmentService();

    private final static Logger logger = LogManager.getLogger(EnrollmentService.class);

    public final static String KEY_OU = "OU";

    private EnrollmentService() {
    }

    public void enrollTei(String teiId) throws NotFoundInDBException, ValidationFailedException {
        TrackedEntityInstance instance = TrackedEntityInstanceService.INSTANCE.getById(teiId);
        Enrollment enrollment = instance.getEnrollment();
        if (enrollment == null) {
            enrollment = new Enrollment();
            enrollment.setOrgUnit(instance.getOrgUnit());
            enrollment.setEnrollment(CodesService.INSTANCE.burrowCode());
            enrollment.setTrackedEntityInstance(instance.getTrackedEntityInstance());
            enrollment.setFresh(true);
        }
        enrollment.setShouldSync(true);
        enrollment.setEnrollmentDate(Calendar.getInstance().getTime());
        enrollment.setStatus(EnrollmentStatus.ACTIVE);
        enrollment.saveToDb(false);
    }

    public Enrollment getForTrackedEntityInstance(TrackedEntityInstance trackedEntityInstance) {
        List<Enrollment> enrollments = Enrollment.find(Enrollment.class, "TRACKED_ENTITY_INSTANCE = ?", trackedEntityInstance.getTrackedEntityInstance());
        if (!enrollments.isEmpty()) {
            return enrollments.get(0);
        }
        return null;
    }

    @Override
    public void syncToServer(final SyncListener syncListener) {
        List<Enrollment> enrollmentsToSync = Enrollment.find(Enrollment.class, "FRESH = ? OR SHOULD_SYNC = ?", "1", "1");

        logger.debug("Starting to sync %d enrollments", enrollmentsToSync.size());

        if (enrollmentsToSync.isEmpty()) {
            syncListener.onDone("Nothing to sync in enrollments");
        }

        final ResponseTracker responseTracker = new ResponseTracker(enrollmentsToSync.size());
        responseTracker.setOnDoneListener(new ResponseTracker.OnDoneListener() {
            @Override
            public void onDone(int total, int failed) {
                if (failed == 0) {
                    syncListener.onDone("Successfully synchronized " + total + " enrollments");
                } else {
                    syncListener.onError(new SyncFailedException(failed + " enrollments out of " + total + " failed to sync"));
                }
            }
        });

        Gson gson = new GsonBuilder().addSerializationExclusionStrategy(new SerializationExclusionStratergy())
                .setDateFormat(DateUtils.getStandardDateFormat().toPattern())
                .create();
        for (final Enrollment enrollment : enrollmentsToSync) {
            try {
                JSONObject jsonObject = new JSONObject(gson.toJson(enrollment));
                JsonObjectPostRequest request = new JsonObjectPostRequest(
                        getPostEndpointUrl(),
                        jsonObject,
                        new VolleyResponseListener<JSONObject>() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                logger.error("Error in syncing enrollment %s", error, enrollment.getEnrollment());
                                responseTracker.requestFailed();
                            }

                            @Override
                            public void onResponse(JSONObject response) {
                                enrollment.setFresh(false);
                                enrollment.setShouldSync(false);
                                try {
                                    enrollment.saveToDb(false);
                                    responseTracker.responseReceived();
                                } catch (ValidationFailedException e) {
                                    logger.error("Error in saving enrollment %s", e, enrollment.getEnrollment());
                                    responseTracker.requestFailed();
                                }
                            }
                        },
                        headers
                );
                fireRequest(request);
            } catch (JSONException e) {
                logger.error("Error occurred when serializing enrollment %s", e, enrollment.getEnrollment());
            }
        }

    }

    @Override
    public void syncFromServer(final SyncListener syncListener) {
        Iterator<OrganizationUnit> organizationUnitIterator = OrganizationUnitService.INSTANCE.getAll();
        ResponseTracker responseTracker = new ResponseTracker((int) OrganizationUnitService.INSTANCE.count());
        responseTracker.setOnDoneListener(new ResponseTracker.OnDoneListener() {
            @Override
            public void onDone(int total, int failed) {
                if (failed == 0) {
                    syncListener.onDone("Enrollments fetched for all organization units");
                } else {
                    syncListener.onError(new SyncFailedException("Failed to fetch enrollments for " + failed + "OrgUnits"));
                }
            }
        });
        while (organizationUnitIterator.hasNext()) {
            OrganizationUnit organizationUnit = organizationUnitIterator.next();
            this.fetchForOu(organizationUnit, syncListener, responseTracker);
        }
    }

    private void fetchForOu(final OrganizationUnit organizationUnit, final SyncListener syncListener, final ResponseTracker responseTracker) {
        GsonRequest<EnrollmentResponse> enrollmentRequest = new GsonRequest<>(
                getSyncEndpointUrl(Collections.singletonMap(KEY_OU, organizationUnit.getOuId())),
                EnrollmentResponse.class,
                headers,
                new Response.Listener<EnrollmentResponse>() {
                    @Override
                    public void onResponse(EnrollmentResponse response) {
                        logger.info("%d enrollments received for ou %s", response.getEnrollments().size(), organizationUnit.getDisplayName());
                        Enrollment.saveInTx(response.getEnrollments());
                        syncListener.onProgress("Received " + response.getEnrollments().size() + " enrollments for " + organizationUnit.getDisplayName());
                        responseTracker.responseReceived();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error("Error occurred while receiving enrollments", error);
                        responseTracker.requestFailed();
                        syncListener.onError(error);
                    }
                }
        );

        fireRequest(enrollmentRequest);
    }

    @Override
    public String getPostEndpointUrl() {
        return this.baseUrl + "enrollments.json?strategy=CREATE_AND_UPDATE";
    }

    @Override
    public String getSyncEndpointQueryString(Map<String, String> params) {
        StringBuilder stringBuilder = new StringBuilder("enrollments.json?skipPaging=true&ou=")
                .append(params.get(KEY_OU));
        return stringBuilder.toString();
    }
}
