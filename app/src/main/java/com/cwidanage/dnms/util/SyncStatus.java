package com.cwidanage.dnms.util;

/**
 * @author Chathura WIdanage
 */

public class SyncStatus {

    private String msg;
    private Type type;

    public SyncStatus(String msg, Type type) {
        this.msg = msg;
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public Type getType() {
        return type;
    }

    public enum Type {
        PROGRESS, ERROR
    }
}
