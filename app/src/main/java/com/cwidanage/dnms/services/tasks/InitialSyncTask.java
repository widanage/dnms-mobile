package com.cwidanage.dnms.services.tasks;

import android.os.AsyncTask;

import com.cwidanage.dnms.DNMSApp;
import com.cwidanage.dnms.entity.Setting;
import com.cwidanage.dnms.services.CodesService;
import com.cwidanage.dnms.services.EnrollmentService;
import com.cwidanage.dnms.services.EventService;
import com.cwidanage.dnms.services.OrganizationUnitService;
import com.cwidanage.dnms.services.SettingService;
import com.cwidanage.dnms.services.SyncListener;
import com.cwidanage.dnms.services.TrackedEntityInstanceService;
import com.cwidanage.dnms.util.ResponseTracker;
import com.cwidanage.dnms.util.logging.LogManager;
import com.cwidanage.dnms.util.logging.Logger;


import java.io.SyncFailedException;
import java.util.Objects;

/**
 * @author Chathura Widanage
 */

public class InitialSyncTask extends AsyncTask<Objects, Boolean, Boolean> {

    private final Logger logger = LogManager.getLogger(InitialSyncTask.class);

    private final String KEY_DONE_INIT_SYNC = "DONE_INIT_SYNC";

    private final ResponseTracker responseTracker;
    private final SyncListener syncListener;

    private final SettingService settingService = SettingService.INSTANCE;

    public InitialSyncTask(final SyncListener syncListener) {
        this.syncListener = syncListener;
        this.responseTracker = new ResponseTracker(4);
        this.responseTracker.setOnDoneListener(new ResponseTracker.OnDoneListener() {
            @Override
            public void onDone(int total, int failed) {
                if (failed == 0) {
                    markAsInitSyncDone();
                } else {
                    syncListener.onError(new SyncFailedException("Failed to perform initial sync"));
                }
            }
        });
    }

    private void markAsInitSyncDone() {
        logger.debug("Marking initial sync status as done");
        settingService.put(KEY_DONE_INIT_SYNC, "TRUE");
        syncListener.onDone("Initial synchronization completed");
    }

    private void syncEvents() {
        EventService.INSTANCE.syncFromServer(new SyncListener() {
            @Override
            public void onDone(String msg) {
                responseTracker.responseReceived();
                syncListener.onProgress("Successfully synchronized events");
            }

            @Override
            public void onProgress(String msg) {
                syncListener.onProgress(msg);
            }

            @Override
            public void onError(Exception e) {
                logger.error("Error in syncing events", e);
                syncListener.onError(e);
            }
        });
    }

    private void syncTeis() {
        TrackedEntityInstanceService.INSTANCE.syncFromServer(new SyncListener() {
            @Override
            public void onDone(String msg) {
                responseTracker.responseReceived();
                syncListener.onProgress("Successfully synchronized TrackedEntityInstances");
            }

            @Override
            public void onProgress(String msg) {
                syncListener.onProgress(msg);
            }

            @Override
            public void onError(Exception e) {
                logger.error("Error in syncing TEIs", e);
                syncListener.onError(e);
            }
        });
    }

    private void fetchCodes() {
        CodesService.INSTANCE.syncFromServer(new SyncListener() {
            @Override
            public void onDone(String msg) {
                responseTracker.responseReceived();
                syncListener.onProgress("Successfully synchronized unique codes");
            }

            @Override
            public void onProgress(String msg) {
                syncListener.onProgress(msg);
            }

            @Override
            public void onError(Exception e) {
                syncListener.onError(e);
            }
        });
    }

    private void syncEnrollments() {
        EnrollmentService.INSTANCE.syncFromServer(new SyncListener() {
            @Override
            public void onDone(String msg) {
                responseTracker.responseReceived();
                syncListener.onProgress("Successfully synchronized enrollments");
            }

            @Override
            public void onProgress(String msg) {
                syncListener.onProgress(msg);
            }

            @Override
            public void onError(Exception e) {
                syncListener.onError(e);
            }
        });
    }


    @Override
    protected Boolean doInBackground(Objects... params) {
        Setting setting = settingService.get(KEY_DONE_INIT_SYNC);
        if (setting != null) {
            logger.debug("Initial syncing has already performed");
            syncListener.onDone(null);
            return true;
        }

        syncListener.onProgress("Starting synchronization task");
        OrganizationUnitService.INSTANCE.syncFromServer(new SyncListener() {
            @Override
            public void onDone(String msg) {
                syncListener.onProgress("Successfully synchronized OrgUnits");
                //parallel syc
                DNMSApp.EXECUTOR.submit(new Runnable() {
                    @Override
                    public void run() {
                        fetchCodes();
                    }
                });
                DNMSApp.EXECUTOR.submit(new Runnable() {
                    @Override
                    public void run() {
                        syncTeis();
                    }
                });
                DNMSApp.EXECUTOR.submit(new Runnable() {
                    @Override
                    public void run() {
                        syncEnrollments();
                    }
                });
                DNMSApp.EXECUTOR.submit(new Runnable() {
                    @Override
                    public void run() {
                        syncEvents();
                    }
                });
            }

            @Override
            public void onProgress(String msg) {
                syncListener.onProgress(msg);
            }

            @Override
            public void onError(Exception e) {
                logger.error("Error in syncing org units", e);
                syncListener.onError(e);
            }
        });
        return Boolean.TRUE;
    }
}