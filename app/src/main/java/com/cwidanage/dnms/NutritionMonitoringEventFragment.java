package com.cwidanage.dnms;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.cwidanage.dnms.constants.DataElement;
import com.cwidanage.dnms.entity.DataValue;
import com.cwidanage.dnms.entity.Event;
import com.cwidanage.dnms.entity.TrackedEntityInstance;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.exceptions.validation.ValidationFailedException;
import com.cwidanage.dnms.listeners.EventCreationListener;
import com.cwidanage.dnms.services.CodesService;
import com.cwidanage.dnms.services.SDCategoryService;
import com.cwidanage.dnms.services.TrackedEntityInstanceService;
import com.cwidanage.dnms.util.DateUtils;
import com.cwidanage.dnms.util.logging.LogManager;
import com.cwidanage.dnms.util.logging.Logger;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author Chathura Widanage
 */
public class NutritionMonitoringEventFragment extends DialogFragment {

    private final static Logger logger = LogManager.getLogger(NutritionMonitoringEventFragment.class);

    private static final String ARG_PARAM1 = "teiId";
    private static final String ARG_PARAM2 = "eventId";
    private static final String ARG_PARAM3 = "eventPosition";

    private String teiId;
    private long eventId = -1;
    private int eventPosition = 0;

    private TrackedEntityInstance trackedEntityInstance;
    private boolean isMale;
    private int ageInMonths;

    private EventCreationListener mListener;

    public NutritionMonitoringEventFragment() {
        // Required empty public constructor
    }

    public static NutritionMonitoringEventFragment newInstance(String param1, long eventId, int position, EventCreationListener listner) {
        NutritionMonitoringEventFragment fragment = new NutritionMonitoringEventFragment();
        fragment.mListener = listner;
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putLong(ARG_PARAM2, eventId);
        args.putInt(ARG_PARAM3, position);
        fragment.setArguments(args);
        return fragment;
    }

    public static NutritionMonitoringEventFragment newInstance(String param1, EventCreationListener listner) {
        return newInstance(param1, -1, 0, listner);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            teiId = getArguments().getString(ARG_PARAM1);
            eventId = getArguments().getLong(ARG_PARAM2);
            eventPosition = getArguments().getInt(ARG_PARAM3);
            try {
                this.trackedEntityInstance = TrackedEntityInstanceService.INSTANCE.getById(teiId);
                this.isMale = trackedEntityInstance.getGender().equals("male");
                this.ageInMonths = (int) (trackedEntityInstance.getAge() * 12);
            } catch (NotFoundInDBException e) {
                //fail
                logger.error("Error in loading tei for tei id %s", e, teiId);
            }
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nutrition_monitoring_event, container, false);

        final Event event = this.eventId != -1 ? Event.findById(Event.class, this.eventId) : new Event();
        List<DataValue> dataValueList = null;
        if (this.eventId == -1) {
            dataValueList = new ArrayList<>();
            event.setDataValues(dataValueList);
            event.setEventDate(Calendar.getInstance().getTime());
            event.setFresh(true);
            event.setEvent(CodesService.INSTANCE.burrowCode());
            event.setTrackedEntityInstance(this.teiId);
            event.setProgramStage(com.cwidanage.dnms.constants.Event.NUTRITION_MONITORING.getId());
        } else {
            try {
                event.loadRelations();
                dataValueList = event.getDataValues();
            } catch (NotFoundInDBException e) {
                logger.error("Error in loading event relations of %s", e, event.getEvent());
                Toast.makeText(this.getContext(), R.string.error_loading_event, Toast.LENGTH_LONG).show();
                this.dismiss();
            }
        }

        Button cancelButton = (Button) view.findViewById(R.id.cancel_btn);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NutritionMonitoringEventFragment.this.dismiss();
            }
        });

        Button saveButton = (Button) view.findViewById(R.id.save_btn);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    event.saveToDb();
                    Toast.makeText(getContext(), R.string.success_saving_event, Toast.LENGTH_LONG).show();
                    mListener.onEventCreated(event, eventId == -1, eventPosition);
                    dismiss();
                } catch (ValidationFailedException e) {
                    Toast.makeText(getContext(), R.string.error_saving_event, Toast.LENGTH_LONG).show();
                }
            }
        });

        //data value map
        HashMap<String, DataValue> dataValueHashMap = new HashMap<>();
        for (DataValue dataValue : event.getDataValues()) {
            dataValueHashMap.put(dataValue.getDataElement(), dataValue);
        }

        //height
        EditText heightTxt = (EditText) view.findViewById(R.id.nm_height_txt);
        TextInputLayout heightTextInputLayout = (TextInputLayout) view.findViewById(R.id.nm_height_input_layout);
        DataValue heightDataValue = new DataValue();
        if (dataValueHashMap.containsKey(DataElement.HEIGHT.getId())) {
            heightDataValue = dataValueHashMap.get(DataElement.HEIGHT.getId());
        } else {
            heightDataValue.setDataElement(DataElement.HEIGHT.getId());
            heightDataValue.setFresh(true);
            dataValueList.add(heightDataValue);
        }
        bindDataValueToEditText(heightDataValue, heightTxt, heightTextInputLayout);

        //weight
        EditText weightTxt = (EditText) view.findViewById(R.id.nm_weight_txt);
        TextInputLayout weightTextInputLayout = (TextInputLayout) view.findViewById(R.id.nm_weight_input_layout);
        DataValue weightDataValue = new DataValue();
        if (dataValueHashMap.containsKey(DataElement.WEIGHT.getId())) {
            weightDataValue = dataValueHashMap.get(DataElement.WEIGHT.getId());
        } else {
            weightDataValue.setDataElement(DataElement.WEIGHT.getId());
            weightDataValue.setFresh(true);
            dataValueList.add(weightDataValue);
        }
        bindDataValueToEditText(weightDataValue, weightTxt, weightTextInputLayout);

        //date
        final EditText dateTxt = (EditText) view.findViewById(R.id.event_date_txt);
        if (eventId == -1) {
            dateTxt.setText(DateUtils.formatFromStandardDate(Calendar.getInstance().getTime()));
            event.setEventDate(Calendar.getInstance().getTime());
        } else {
            dateTxt.setText(DateUtils.formatFromStandardDate(event.getEventDate()));
        }
        dateTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                try {
                    Date parsedDate = DateUtils.parseToStandardDate(dateTxt.getText().toString());
                    calendar.setTime(parsedDate);
                } catch (ParseException e) {
                    Log.e("TEI_PROFILE", "Error in parsing date", e);
                    //ignore
                }
                new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String dateString = DateUtils.formatFromStandardDate(calendar.getTime());
                        dateTxt.setText(dateString);
                        event.setEventDate(calendar.getTime());
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        return view;
    }

    private void setColorToField(final int color, final EditText editText, final TextInputLayout textInputLayout) {
        getActivity().runOnUiThread(new Runnable() {
            @SuppressLint("ResourceType")
            @Override
            public void run() {
                if (color == R.color.white) {
                    editText.setTextColor(getResources().getColor(R.color.black));
                } else {
                    editText.setHintTextColor(getResources().getColor(color));
                }
                textInputLayout.setBackgroundColor(getResources().getColor(color));
            }
        });
    }

    private void changeBackgroundColor(final String txt, final DataValue dataValue, final EditText editText, final TextInputLayout textInputLayout) {
        DNMSApp.EXECUTOR.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    float value = Float.parseFloat(txt);
                    if (dataValue.getDataElement().equals(DataElement.HEIGHT.getId())) {
                        setColorToField(SDCategoryService.INSTANCE.getHeightCategory(isMale, ageInMonths, value).getColor(), editText, textInputLayout);
                    } else if (dataValue.getDataElement().equals(DataElement.WEIGHT.getId())) {
                        setColorToField(SDCategoryService.INSTANCE.getWeightCategory(isMale, ageInMonths, value).getColor(), editText, textInputLayout);
                    }
                } catch (Exception e) {
                    setColorToField(R.color.white, editText, textInputLayout);
                    //do nothing
                }
            }
        });

    }

    private void bindDataValueToEditText(final DataValue dataValue, final EditText editText, final TextInputLayout textInputLayout) {
        final String oldValue = dataValue.getValue();
        if (oldValue != null) {
            editText.setText(oldValue);
            changeBackgroundColor(oldValue, dataValue, editText, textInputLayout);
        }
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (oldValue != null && s.toString().equals(oldValue)) {
                    dataValue.setShouldSync(false);
                } else {
                    dataValue.setValue(s.toString());
                    dataValue.setShouldSync(true);
                }
                changeBackgroundColor(s.toString(), dataValue, editText, textInputLayout);
            }
        });
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
