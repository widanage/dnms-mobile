package com.cwidanage.dnms.services.requests;

import com.android.volley.Response;

/**
 * @author Chathura Widanage
 */

public interface VolleyResponseListener<T> extends Response.Listener<T>, Response.ErrorListener {

}
