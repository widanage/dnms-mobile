package com.cwidanage.dnms.services.requests;

import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.Map;

/**
 * @author Chathura Widanage
 */

public class DeleteRequest extends JsonObjectRequest {

    private Map<String, String> headers;

    public DeleteRequest(String url, VolleyResponseListener<JSONObject> listener, Map<String, String> headers) {
        super(Method.DELETE, url, new JSONObject(), listener, listener);
        this.headers = headers;
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }
}
