package com.cwidanage.dnms.util.logging;

/**
 * @author Chathura Widanage
 */

public class Logger {

    private String tag;

    public Logger(String tag) {
        this.tag = tag;
    }

    public void info(String msg, Object... args) {
        Log log = new Log(LogLevel.INFO, tag, msg, args);
        AsyncLogger.INSTANCE.submit(log);
    }

    public void debug(String msg, Object... args) {
        Log log = new Log(LogLevel.DEBUG, tag, msg, args);
        AsyncLogger.INSTANCE.submit(log);
    }

    public void error(String msg, Object... args) {
        Log log = new Log(LogLevel.ERROR, tag, msg, args);
        AsyncLogger.INSTANCE.submit(log);
    }

    public void error(String msg, Throwable throwable, Object... args) {
        Log log = new Log(LogLevel.ERROR, tag, msg, args);
        log.setThrowable(throwable);
        AsyncLogger.INSTANCE.submit(log);
    }
}
