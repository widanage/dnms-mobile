package com.cwidanage.dnms.constants;

import com.cwidanage.dnms.R;

/**
 * @author Chathura Widanage
 */

public enum Gender {

    MALE(R.string.male, "male"), FEMALE(R.string.female, "female"), OTHER(R.string.other, "Other");

    private String value;
    private int stringId;

    Gender(int stringId, String value) {
        this.stringId = stringId;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getStringId() {
        return stringId;
    }

    public void setStringId(int stringId) {
        this.stringId = stringId;
    }
}
