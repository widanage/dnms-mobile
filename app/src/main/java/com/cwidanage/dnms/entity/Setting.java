package com.cwidanage.dnms.entity;

import com.orm.SugarRecord;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

/**
 * @author Chathura Widanage
 */
@Table
public class Setting extends SugarRecord {

    @Unique
    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void saveToDb() {
        Setting.update(this);
    }
}
