package com.cwidanage.dnms;

import android.util.Log;

import com.cwidanage.dnms.services.AbstractService;
import com.orm.SugarApp;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author Chathura Widanage
 */

public class DNMSApp extends SugarApp {

    public static ExecutorService EXECUTOR = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    @Override
    public void onCreate() {
        super.onCreate();
        AbstractService.init(getApplicationContext());
    }
}
