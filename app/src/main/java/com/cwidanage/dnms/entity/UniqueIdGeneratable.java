package com.cwidanage.dnms.entity;

/**
 * @author Chathura Widanage
 */

public interface UniqueIdGeneratable {
    void generateUniqueId();
}
