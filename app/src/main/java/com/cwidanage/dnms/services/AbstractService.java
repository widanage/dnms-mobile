package com.cwidanage.dnms.services;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.cwidanage.dnms.entity.Setting;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Chathura Widanage
 */

public abstract class AbstractService {

    protected final static String KEY_AUTH = "KEY_AUTH";
    protected final static String PAGE_PARAM = "PAGE_PARAM";

    protected HashMap<String, String> headers = new HashMap<>();
    //protected String baseUrl = "http://dhis.pgim.cmb.ac.lk/nss/api/";
    protected String baseUrl = "http://lankanets.info:8080/nss/api/";
    protected Context context;

    private static RequestQueue requestQueue;

    private SettingService settingService = SettingService.INSTANCE;

    protected final int MAX_RETRIES = 7;

    protected AbstractService() {
        Setting setting = settingService.get(KEY_AUTH);
        if (setting != null) {
            this.setAuthHeader(setting.getValue());
        }
        //headers.put("Authorization", "Basic cGhtNDpwaG1QSE0xMjM=");
        //headers.put("Authorization", "Basic Y2hhdGh1cmE6Q2hhdGh1cmExMjM=");
    }

    public static void init(Context context) {
        requestQueue = Volley.newRequestQueue(context);
    }

    protected void setAuthHeader(String token) {
        headers.put("Authorization", "Basic " + token);
    }


    public void fireRequest(Request request) {
        request.setRetryPolicy(new DefaultRetryPolicy(
                900000,//15 minutes
                MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    public abstract void syncFromServer(SyncListener syncListener);

    public void syncToServer(SyncListener syncListener) {

    }

    public abstract String getSyncEndpointQueryString(Map<String, String> params);

    public String getSyncEndpointUrl(Map<String, String> params) {
        return this.baseUrl + this.getSyncEndpointQueryString(params);
    }

    public String getDeleteEndpointUrl(String id) {
        throw new UnsupportedOperationException();
    }

    public String getPostEndpointUrl() {
        throw new UnsupportedOperationException();
    }

    public class RequestContext {

        private int sent = 0;
        private int received = 0;

        public synchronized void sent() {
            this.sent++;
        }

        public synchronized void recieved() {
            this.received++;
        }

        public synchronized boolean isDone() {
            return this.sent == received || this.sent < received;
        }
    }
}

