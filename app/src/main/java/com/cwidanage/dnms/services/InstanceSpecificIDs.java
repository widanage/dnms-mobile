package com.cwidanage.dnms.services;

import com.cwidanage.dnms.constants.DataElement;

import java.util.HashMap;

/**
 * @author Chathura Widanage
 */

public class InstanceSpecificIDs {
    public final static String TRACKED_ENTITY = "kuWSv1dRXIm";
    public final static String PHM_USER_ROLE = "jpsN0Kh6KTr";
    public final static String PROGRAM = "MmGml1Gyb7K";

    public final static String PROGRAM_STAGE_NUT_MONITORING = "wS2i9c9hXXz";
    public final static String PROGRAM_STAGE_RISK_ID = "vTWcDsFE1rf";
    public final static String PROGRAM_STAGE_REGISTRATION = "FyR0ymIDsoA";

    public static HashMap<String, DataElement> dataElementMap = new HashMap<>();

    static {
        for (DataElement dataElement : DataElement.values()) {
            dataElementMap.put(dataElement.getId(), dataElement);
        }
    }
}
