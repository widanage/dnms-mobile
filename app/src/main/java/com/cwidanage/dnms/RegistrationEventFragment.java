package com.cwidanage.dnms;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.Toast;

import com.cwidanage.dnms.adapters.SelectableItemHolder;
import com.cwidanage.dnms.constants.DataElement;
import com.cwidanage.dnms.constants.RegistrationDataElements;
import com.cwidanage.dnms.entity.DataValue;
import com.cwidanage.dnms.entity.Event;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.exceptions.validation.ValidationFailedException;
import com.cwidanage.dnms.listeners.EventCreationListener;
import com.cwidanage.dnms.services.CodesService;
import com.cwidanage.dnms.services.EnrollmentService;
import com.cwidanage.dnms.services.TrackedEntityInstanceService;
import com.cwidanage.dnms.util.logging.LogManager;
import com.cwidanage.dnms.util.logging.Logger;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class RegistrationEventFragment extends DialogFragment {

    private final Logger logger = LogManager.getLogger(RegistrationEventFragment.class);

    private static final String ARG_PARAM1 = "teiId";
    private static final String ARG_PARAM2 = "eventId";
    private static final String ARG_PARAM3 = "eventPosition";

    private String teiId;
    private Long eventId;
    private int eventPosition = 0;

    private EventCreationListener eventCreationListener;

    public RegistrationEventFragment() {
        // Required empty public constructor
    }

    public static RegistrationEventFragment newInstance(String teiId, EventCreationListener eventCreationListener) {
        return newInstance(teiId, -1, 0, eventCreationListener);
    }

    public static RegistrationEventFragment newInstance(String teiId, long eventId, int position, EventCreationListener eventCreationListener) {
        RegistrationEventFragment fragment = new RegistrationEventFragment();
        fragment.eventCreationListener = eventCreationListener;
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, teiId);
        args.putLong(ARG_PARAM2, eventId);
        args.putLong(ARG_PARAM3, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        /*Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            teiId = getArguments().getString(ARG_PARAM1);
            eventId = getArguments().getLong(ARG_PARAM2);
            eventPosition = getArguments().getInt(ARG_PARAM3);
        }
    }

    private void showSaveFeedback(final int stringId, final boolean dismiss) {
        this.showSaveFeedback(getString(stringId), dismiss);
    }

    private void showSaveFeedback(final String toastText, final boolean dismiss) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(), toastText, Toast.LENGTH_LONG).show();
                if (dismiss) {
                    dismiss();
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final Event event = this.eventId != -1 ? Event.findById(Event.class, this.eventId) : new Event();
        List<DataValue> dataValueList = null;
        if (this.eventId == -1) {
            dataValueList = new ArrayList<>();
            event.setDataValues(dataValueList);
            event.setEventDate(Calendar.getInstance().getTime());
            event.setFresh(true);
            event.setEvent(CodesService.INSTANCE.burrowCode());
            event.setTrackedEntityInstance(this.teiId);
            event.setProgramStage(com.cwidanage.dnms.constants.Event.REGISTRATION.getId());
        } else {
            try {
                event.loadRelations();
                dataValueList = event.getDataValues();
            } catch (NotFoundInDBException e) {
                logger.error("Error in loading event relations of %s", e, event.getEvent());
                Toast.makeText(this.getContext(), R.string.error_loading_event, Toast.LENGTH_LONG).show();
                this.dismiss();
            }
        }
        event.setShouldSync(true);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_registration_event, container, false);

        Button cancelButton = (Button) view.findViewById(R.id.cancel_btn);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Button saveButton = (Button) view.findViewById(R.id.save_btn);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DNMSApp.EXECUTOR.submit(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            //--- ENROLLMENT ---//
                            EnrollmentService.INSTANCE.enrollTei(teiId);

                            if (eventId != -1) {
                                Event oldEvent = Event.findById(Event.class, eventId);
                                event.setId(oldEvent.getId());
                                event.setEvent(oldEvent.getEvent());
                            }

                            //--- EVENT SAVING ---//
                            event.saveToDb();
                            eventCreationListener.onEventCreated(event, eventId == -1, eventPosition);
                            showSaveFeedback(R.string.success_saving_event, true);
                        } catch (ValidationFailedException e) {
                            logger.error("Validation error occurred when saving registration event of TEI %s", e, teiId);
                            showSaveFeedback(e.getStringId(), false);
                        } catch (NotFoundInDBException e) {
                            logger.error("Error occurred when enrolling TEI %s", e, teiId);
                            showSaveFeedback(e.getMessage(), false);
                        }
                    }
                });

            }
        });

        //esiting data value map
        HashMap<String, DataValue> dataValueHashMap = new HashMap<>();
        for (DataValue dataValue : event.getDataValues()) {
            dataValueHashMap.put(dataValue.getDataElement(), dataValue);
        }


        ScrollView optionsList = (ScrollView) view.findViewById(R.id.options_list);
        TreeNode root = TreeNode.root();
        for (DataElement subRisk : RegistrationDataElements.SET) {
            DataValue subRiskDataValue;
            if (dataValueHashMap.containsKey(subRisk.getId())) {
                subRiskDataValue = dataValueHashMap.get(subRisk.getId());
            } else {
                subRiskDataValue = new DataValue();
                subRiskDataValue.setDataElement(subRisk.getId());
                dataValueList.add(subRiskDataValue);
            }

            TreeNode child0 = new TreeNode(getContext().getString(subRisk.getStringId()));
            child0.setSelectable(true);
            child0.setViewHolder(new SelectableItemHolder(getActivity(), subRiskDataValue));
            child0.setSelected(subRiskDataValue.getValue() != null && subRiskDataValue.getValue().equals("true"));
            root.addChild(child0);
        }
        AndroidTreeView tView = new AndroidTreeView(getActivity(), root);
        tView.setDefaultAnimation(true);
        optionsList.addView(tView.getView());
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
