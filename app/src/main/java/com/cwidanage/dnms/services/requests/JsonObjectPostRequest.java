package com.cwidanage.dnms.services.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.Map;

/**
 * @author Chathura Widanage
 */

public class JsonObjectPostRequest extends JsonObjectRequest {

    private Map<String, String> headers;

    public JsonObjectPostRequest(String url, JSONObject jsonRequest, VolleyResponseListener<JSONObject> listener, Map<String, String> headers) {
        super(Method.POST, url, jsonRequest, listener, listener);
        this.headers = headers;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return this.headers;
    }
}
