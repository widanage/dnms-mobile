package com.cwidanage.dnms.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cwidanage.dnms.R;
import com.cwidanage.dnms.entity.DataValue;

import java.util.List;

/**
 * @author Chathura Widanage
 */

public class EventDataValueListAdapter extends ArrayAdapter<DataValue> {

    private List<DataValue> dataValues;

    public EventDataValueListAdapter(@NonNull Context context, @NonNull List<DataValue> objects) {
        super(context, R.layout.event_preview_list_item, objects);
        this.dataValues = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.event_preview_list_item, parent, false);
        TextView value = (TextView) rowView.findViewById(R.id.data_value);
        ImageView image = (ImageView) rowView.findViewById(R.id.data_value_icon);

        DataValue dataValue = this.dataValues.get(position);
        value.setText(dataValue.getValue());
        return rowView;
    }
}
