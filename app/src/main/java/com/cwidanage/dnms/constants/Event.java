package com.cwidanage.dnms.constants;

/**
 * @author Chathura Widanage
 */

public enum Event {
    REGISTRATION("FyR0ymIDsoA"), RISK_IDENTIFICATION("vTWcDsFE1rf"), NUTRITION_MONITORING("wS2i9c9hXXz");

    private String id;

    Event(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
