package com.cwidanage.dnms.entity;

import android.util.Log;

import com.cwidanage.dnms.R;
import com.cwidanage.dnms.constants.TrackedEntityAttribute;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.exceptions.validation.ValidationFailedException;
import com.cwidanage.dnms.services.EnrollmentService;
import com.cwidanage.dnms.services.EventService;
import com.cwidanage.dnms.services.InstanceSpecificIDs;
import com.cwidanage.dnms.services.OrganizationUnitService;
import com.cwidanage.dnms.util.DateUtils;
import com.cwidanage.dnms.util.JsonIgnore;
import com.cwidanage.dnms.util.ValidationUtils;
import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;
import com.orm.SugarTransactionHelper;
import com.orm.dsl.Ignore;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author Chathura Widanage
 */
@Table
public class TrackedEntityInstance extends AbstractEntity {

    @Unique
    private String trackedEntityInstance;

    @Ignore
    private String trackedEntity = InstanceSpecificIDs.TRACKED_ENTITY;

    private Date lastUpdated;

    //--- ATTRIBUTES ---//
    @Ignore
    private List<TrackedEntityInstanceAttribute> attributes;

    @JsonIgnore
    @Expose
    @Ignore
    private HashMap<String, TrackedEntityInstanceAttribute> attributesMap = new HashMap<>();

    //--- ORG UNIT ---//
    private String orgUnit;

    @JsonIgnore
    @Expose
    @Ignore
    private OrganizationUnit organizationUnit;

    //--- EVENTS ---//
    @JsonIgnore
    @Expose
    @Ignore
    private List<Event> events;

    //--- ENROLLMENTS ---//
    @JsonIgnore
    @Ignore
    private Enrollment enrollment;

    public Enrollment getEnrollment() {
        return enrollment;
    }

    public String getTrackedEntity() {
        return trackedEntity;
    }

    public String getTrackedEntityInstance() {
        return trackedEntityInstance;
    }

    public void setTrackedEntityInstance(String trackedEntityInstance) {
        this.trackedEntityInstance = trackedEntityInstance;
    }

    public String getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(String orgUnit) {
        this.orgUnit = orgUnit;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public List<TrackedEntityInstanceAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<TrackedEntityInstanceAttribute> attributes) {
        this.attributes = attributes;
    }

    public OrganizationUnit getOrganizationUnit() {
        return organizationUnit;
    }

    public void setOrganizationUnit(OrganizationUnit organizationUnit) {
        this.organizationUnit = organizationUnit;
    }

    //--- Processed methods ---//
    public String getFullName() {
        String firstName = this.getOrDefaultAttributeValue(TrackedEntityAttribute.FIRST_NAME.getId(), "");
        String otherNames = this.getOrDefaultAttributeValue(TrackedEntityAttribute.OTHER_NAMES.getId(), "");
        String lastName = this.getOrDefaultAttributeValue(TrackedEntityAttribute.LAST_NAME.getId(), "");
        StringBuilder nameBuilder = new StringBuilder();
        if (!firstName.isEmpty()) {
            nameBuilder.append(firstName);
            nameBuilder.append(" ");
        }

        if (!otherNames.isEmpty()) {
            nameBuilder.append(otherNames);
            nameBuilder.append(" ");
        }

        if (!lastName.isEmpty()) {
            nameBuilder.append(lastName);
        }
        return nameBuilder.toString();
    }

    public float getAge() {
        String dobString = this.getOrDefaultAttributeValue(TrackedEntityAttribute.DOB.getId(), null);
        if (dobString == null || dobString.isEmpty()) {
            return 0f;
        }

        try {
            Date date = DateUtils.parseToStandardDate(dobString);
            Date now = Calendar.getInstance().getTime();
            return DateUtils.getDateDifferenceInYears(now, date);
        } catch (ParseException e) {
            return 0f;
        }
    }

    public String getGender() {
        return this.getOrDefaultAttributeValue(TrackedEntityAttribute.GENDER.getId(), "undefined");
    }

    //--- end of processed methods ---//

    public void reloadAttributesMap() {
        this.attributesMap.clear();
        for (TrackedEntityInstanceAttribute attribute : this.attributes) {
            this.attributesMap.put(attribute.getAttribute(), attribute);
        }
    }

    public void loadEvents() {
        //events
        this.events = EventService.INSTANCE.getByTrackedEntityInstanceId(this.trackedEntityInstance);
        this.sortEvents();
    }

    private void forceLoadRelations() throws NotFoundInDBException {
        //attributes
        this.attributes = SugarRecord.find(TrackedEntityInstanceAttribute.class, "TEI_ID = ?", this.trackedEntityInstance);
        this.reloadAttributesMap();

        //organization unit
        OrganizationUnitService organizationUnitService = OrganizationUnitService.INSTANCE;
        if (this.orgUnit != null) {
            this.organizationUnit = organizationUnitService.getById(this.orgUnit);
        }

        //events
        this.loadEvents();

        //enrollment
        this.enrollment = EnrollmentService.INSTANCE.getForTrackedEntityInstance(this);
    }

    @Override
    public void loadRelations() throws NotFoundInDBException {
        if (isRelationsLoaded()) {
            return;
        }
        this.forceLoadRelations();
        setRelationsLoaded();
    }

    public void sortEvents() {
        Collections.sort(this.events, new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
                return o2.getEventDate().compareTo(o1.getEventDate());
            }
        });
    }

    private void validate() throws ValidationFailedException {
        //ou validation
        if (this.getOrgUnit() == null) {
            throw new ValidationFailedException(R.string.error_invalid_gn);
        }

        //db validation
        String firstName = this.getOrDefaultAttributeValue(TrackedEntityAttribute.FIRST_NAME.getId(), null);
        String otherNames = this.getOrDefaultAttributeValue(TrackedEntityAttribute.OTHER_NAMES.getId(), null);
        String lastName = this.getOrDefaultAttributeValue(TrackedEntityAttribute.LAST_NAME.getId(), null);
        if (ValidationUtils.isEmptyString(firstName) &&
                ValidationUtils.isEmptyString(otherNames) && ValidationUtils.isEmptyString(lastName)) {
            throw new ValidationFailedException(R.string.error_name_validation);
        }

        String dob = this.getOrDefaultAttributeValue(TrackedEntityAttribute.DOB.getId(), null);
        if (ValidationUtils.isEmptyString(dob)) {
            throw new ValidationFailedException(R.string.error_dob_validation);
        }

        String gender = this.getOrDefaultAttributeValue(TrackedEntityAttribute.GENDER.getId(), null);
        if (ValidationUtils.isEmptyString(gender)) {
            throw new ValidationFailedException(R.string.error_gender_validation);
        }

        String weight = this.getOrDefaultAttributeValue(TrackedEntityAttribute.BIRTH_WEIGHT.getId(), null);
        if (!ValidationUtils.isValidNumber(weight)) {
            throw new ValidationFailedException(R.string.error_weight_validation);
        }

    }

    @Override
    public void saveToDb(boolean noValidation) throws ValidationFailedException {
        if (!noValidation) {
            validate();
        }

        this.setTemporary(false);
        if (attributes != null) {
            for (TrackedEntityInstanceAttribute attribute : attributes) {
                attribute.setTeiId(this.getTrackedEntityInstance());
                attribute.generateUniqueId();
                if (attribute.isShouldSync() || attribute.isFresh()) {
                    this.setShouldSync(true);
                }
                attribute.saveToDb(noValidation);
            }
        }

        TrackedEntityInstance.update(this);

        try {
            this.forceLoadRelations();
        } catch (NotFoundInDBException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setSynced() {
        super.setSynced();
        for (TrackedEntityInstanceAttribute attribute : this.attributes) {
            attribute.setSynced();
        }
        TrackedEntityInstance.update(this);
    }

    public HashMap<String, TrackedEntityInstanceAttribute> getAttributesMap() {
        return attributesMap;
    }

    public String getOrDefaultAttributeValue(String attributeId, String defaultValue) {
        try {
            this.loadRelations();
        } catch (NotFoundInDBException e) {
            Log.e("NOT_FOUND", e.getMessage());
            //do nothing
        }
        if (!this.attributesMap.containsKey(attributeId) || this.attributesMap.get(attributeId).getValue() == null) {
            return defaultValue;
        }
        return this.attributesMap.get(attributeId).getValue();
    }

    @Override
    public String toString() {
        return "TrackedEntityInstance{" +
                "trackedEntityInstance='" + trackedEntityInstance + '\'' +
                ", lastUpdated=" + lastUpdated +
                ", attributes=" + attributes +
                '}';
    }

    public List<Event> getEvents() {
        return this.events;
    }
}
