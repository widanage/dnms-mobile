package com.cwidanage.dnms.util.logging;

/**
 * @author Chathura Widanage
 */

public class Log {

    private LogLevel logLevel;

    private String tag;
    private String log;
    private Object[] args;
    private Throwable throwable;

    public Log(LogLevel logLevel, String tag, String log, Object... args) {
        this.logLevel = logLevel;
        this.tag = tag;
        this.log = log;
        this.args = args;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public String getTag() {
        return tag;
    }

    public LogLevel getLogLevel() {
        return logLevel;
    }

    public String getLog() {
        return log;
    }

    public Object[] getArgs() {
        return args;
    }
}
