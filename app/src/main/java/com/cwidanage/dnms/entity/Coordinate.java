package com.cwidanage.dnms.entity;

import com.cwidanage.dnms.util.JsonIgnore;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

/**
 * @author Chathura Widanage
 */
@Table
public class Coordinate extends AbstractEntity {

    private double latitude;
    private double longitude;

    @JsonIgnore
    @Unique
    private String eventId;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public void saveToDb(boolean noValidation) {
        Coordinate.update(this);
        this.setShouldSync(true);
    }

    @Override
    public void setSynced() {
        super.setSynced();
        Coordinate.update(this);
    }
}
