package com.cwidanage.dnms;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.cwidanage.dnms.adapters.SyncStatusAdapter;
import com.cwidanage.dnms.services.SyncListener;
import com.cwidanage.dnms.services.SyncService;
import com.cwidanage.dnms.services.TrackedEntityInstanceService;
import com.cwidanage.dnms.util.SyncStatus;
import com.cwidanage.dnms.util.logging.LogManager;
import com.cwidanage.dnms.util.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class LoadingActivity extends AppCompatActivity {

    public static final String TYPE_UP_SYNC = "UP_SYNC";

    private final static Logger logger = LogManager.getLogger(LoadingActivity.class);

    private List<SyncStatus> syncStatuses = new ArrayList<>();
    private SyncStatusAdapter syncListenerAdapter;
    private RecyclerView recyclerView;

    private ImageButton nextBtn;
    private ImageButton retryBtn;
    private TextView titleTxt;

    private boolean isUpSync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        titleTxt = (TextView) findViewById(R.id.title_txt);
        retryBtn = (ImageButton) findViewById(R.id.retry_btn);
        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sync();
            }
        });

        nextBtn = (ImageButton) findViewById(R.id.next_btn);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMainActivity();
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView = (RecyclerView) findViewById(R.id.statusList);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        syncListenerAdapter = new SyncStatusAdapter(syncStatuses);
        recyclerView.setAdapter(syncListenerAdapter);
        TrackedEntityInstanceService.INSTANCE.clearTemp();

        this.isUpSync = getIntent().getBooleanExtra(TYPE_UP_SYNC, false);
        this.sync();
    }

    private void sync() {
        retryBtn.setVisibility(View.GONE);
        nextBtn.setVisibility(View.GONE);
        this.syncStatuses.clear();
        this.syncListenerAdapter.notifyDataSetChanged();
        this.titleTxt.setText("SYNCHRONIZING");
        if (this.isUpSync) {
            startUpSync();
        } else {
            startInitSyncing();
        }
    }

    private void loadMainActivity() {
        Intent i = new Intent(LoadingActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    private void startUpSync() {
        SyncService.INSTANCE.doUpSync(new LoadingListener());
    }

    private void publishSyncStatus(final SyncStatus syncStatus) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                synchronized (syncStatuses) {
                    syncStatuses.add(syncStatus);
                    syncListenerAdapter.notifyItemInserted(syncStatuses.size() - 1);
                }
            }
        });
    }

    private void startInitSyncing() {
        SyncService.INSTANCE.doInitSync(new LoadingListener());
    }

    public class LoadingListener implements SyncListener {
        @Override
        public void onDone(String msg) {
            if (msg == null) {
                loadMainActivity();
            }
            SyncStatus syncStatus = new SyncStatus(msg, SyncStatus.Type.PROGRESS);
            publishSyncStatus(syncStatus);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    nextBtn.setVisibility(View.VISIBLE);
                    titleTxt.setText("SYNCHRONIZED :)");
                }
            });
        }

        @Override
        public void onProgress(final String msg) {
            SyncStatus syncStatus = new SyncStatus(msg, SyncStatus.Type.PROGRESS);
            publishSyncStatus(syncStatus);
        }

        @Override
        public void onError(Exception e) {
            logger.error("Error in syncing", e);
            SyncStatus syncStatus = new SyncStatus(e.getMessage(), SyncStatus.Type.ERROR);
            publishSyncStatus(syncStatus);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    retryBtn.setVisibility(View.VISIBLE);
                    if (isUpSync) {
                        nextBtn.setVisibility(View.VISIBLE);
                    } else {
                        nextBtn.setVisibility(View.GONE);
                    }
                    titleTxt.setText("SYNC FAILED :(");
                }
            });
        }
    }
}
