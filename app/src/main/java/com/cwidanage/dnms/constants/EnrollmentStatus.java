package com.cwidanage.dnms.constants;

/**
 * @author Chathura Widanage
 */
public enum EnrollmentStatus {
    ACTIVE, COMPLETED, CANCELLED
}
