package com.cwidanage.dnms.services.models;

import com.cwidanage.dnms.entity.Code;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Chathura Widanage
 */

public class CodesResponse {

    private List<String> codes = new ArrayList<>();

    public List<String> getCodes() {
        return codes;
    }

    public void setCodes(List<String> codes) {
        this.codes = codes;
    }
}
