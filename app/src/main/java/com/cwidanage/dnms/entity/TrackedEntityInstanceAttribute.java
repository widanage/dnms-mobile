package com.cwidanage.dnms.entity;

import com.cwidanage.dnms.util.JsonIgnore;
import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

import java.util.Date;

/**
 * @author Chathura Widanage
 */
@Table
public class TrackedEntityInstanceAttribute extends AbstractEntity implements UniqueIdGeneratable {

    @JsonIgnore
    @Unique
    private String uniqueId;
    private String attribute;
    private String value;
    private Date lastUpdated;

    @JsonIgnore
    private String teiId;

    @JsonIgnore
    @Expose
    @Ignore
    private TrackedEntityInstance trackedEntityInstance;

    public String getTeiId() {
        return teiId;
    }

    public void setTeiId(String teiId) {
        this.teiId = teiId;
    }

    @Override
    public void generateUniqueId() {
        this.uniqueId = this.teiId + "_" + this.attribute;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public TrackedEntityInstance getTrackedEntityInstance() {
        return trackedEntityInstance;
    }

    public void setTrackedEntityInstance(TrackedEntityInstance trackedEntityInstance) {
        this.trackedEntityInstance = trackedEntityInstance;
    }

    @Override
    public void loadRelations() {
        this.trackedEntityInstance = SugarRecord.find(TrackedEntityInstance.class, "TRACKED_ENTITY_INSTANCE = ?", this.teiId).get(0);
    }

    @Override
    public void saveToDb(boolean noValidation) {
        if (this.isFresh() && (this.getValue() == null || this.getValue().isEmpty())) {
            return;
        }
        TrackedEntityInstanceAttribute.update(this);
    }

    @Override
    public void setSynced() {
        super.setSynced();
        TrackedEntityInstance.update(this);
    }

    @Override
    public String toString() {
        return "TrackedEntityInstanceAttribute{" +
                "attribute='" + attribute + '\'' +
                ", value='" + value + '\'' +
                ", lastUpdated=" + lastUpdated +
                '}';
    }
}
