package com.cwidanage.dnms.services;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.rest.OrganizationUnit;
import com.cwidanage.dnms.services.models.OrganizationUnitResponse;
import com.cwidanage.dnms.services.requests.GsonRequest;
import com.orm.SugarRecord;
import com.orm.SugarTransactionHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Chathura Widanage
 */

public class OrganizationUnitService extends AbstractService {

    public final static OrganizationUnitService INSTANCE = new OrganizationUnitService();

    private OrganizationUnitService() {

    }

    @Override
    public void syncFromServer(final SyncListener syncListener) {
        syncListener.onProgress("Starting ou synchronization");
        final GsonRequest<OrganizationUnitResponse> request
                = new GsonRequest<>(getSyncEndpointUrl(null), OrganizationUnitResponse.class,
                headers, new Response.Listener<OrganizationUnitResponse>() {
            @Override
            public void onResponse(OrganizationUnitResponse response) {
                Log.d("STATUS", "OrgUnit response received");
                final List<OrganizationUnit> orgUnitsRest = response.getOrganisationUnits();
                SugarTransactionHelper.doInTransaction(new SugarTransactionHelper.Callback() {
                    @Override
                    public void manipulateInTransaction() {
                        for (OrganizationUnit ou : orgUnitsRest) {
                            ou.getEntity().saveToDb(true);
                        }
                        syncListener.onProgress("Organization units synchronized");
                        syncListener.onDone(null);
                    }
                });
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
                syncListener.onError(error);
            }
        });
        this.fireRequest(request);
    }

    public long count() {
        return com.cwidanage.dnms.entity.OrganizationUnit.count(com.cwidanage.dnms.entity.OrganizationUnit.class);
    }

    public Iterator<com.cwidanage.dnms.entity.OrganizationUnit> getAll() {
        return SugarRecord.findAll(com.cwidanage.dnms.entity.OrganizationUnit.class);
    }

    public com.cwidanage.dnms.entity.OrganizationUnit getById(String orgId) throws NotFoundInDBException {
        List<com.cwidanage.dnms.entity.OrganizationUnit> withQuery = SugarRecord.find(com.cwidanage.dnms.entity.OrganizationUnit.class, "OU_ID = ?", orgId);
        if (withQuery.isEmpty()) {
            throw new NotFoundInDBException("Couldn't find an organization unit with id " + orgId);
        }
        return withQuery.get(0);
    }

    @Override
    public String getSyncEndpointQueryString(Map<String, String> params) {
        return "organisationUnits.json?userOnly=true&paging=false";
    }
}
