package com.cwidanage.dnms.constants;

import com.cwidanage.dnms.R;

/**
 * @author Chathura Widanage
 */

public enum ThriposhaStatus {

    RECEIVING(R.string.receiving, "Receiving"), NOT_RECEIVING(R.string.not_receiving, "Not Receiving"),
    RECEIVED_IN_PAST(R.string.received_in_past, "Received in Past"),
    NO_NEED_TO_RECEIVE(R.string.no_need_to_receive, "No need to Receive");

    private int stringId;
    private String value;

    ThriposhaStatus(int stringId, String value) {
        this.stringId = stringId;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getStringId() {
        return stringId;
    }

    public void setStringId(int stringId) {
        this.stringId = stringId;
    }
}
