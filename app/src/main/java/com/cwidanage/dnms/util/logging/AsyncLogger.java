package com.cwidanage.dnms.util.logging;

import android.support.annotation.NonNull;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadFactory;

/**
 * @author Chathura Widanage
 */
public class AsyncLogger {

    public static final AsyncLogger INSTANCE = new AsyncLogger();

    private BlockingDeque<Log> logs = new LinkedBlockingDeque<>();

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd hh:mm:ss");

    private File logRoot = new File("/sdcard/Android/data/DNMS/");
    private File logFile = new File(logRoot, simpleDateFormat.format(new Date()));

    private AsyncLogger() {

        if (!logFile.exists()) {
            try {
                logRoot.mkdirs();
                android.util.Log.d("LOGGER", "Creating log file");
                logFile.createNewFile();
            } catch (IOException e) {
                //do nothing
                android.util.Log.d("LOGGER", "Failed to create logging file");
            }
        }

        Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(@NonNull Runnable r) {
                Thread thread = new Thread(r);
                thread.setPriority(Thread.MIN_PRIORITY);
                return thread;
            }
        }).submit(new Runnable() {
            private String prepareLog(Log log) {
                try {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append('[')
                            .append(simpleDateFormat.format(Calendar.getInstance().getTime()))
                            .append("]");
                    stringBuilder.append('[').append(log.getLogLevel()).append("]");
                    stringBuilder.append('[')
                            .append(log.getTag())
                            .append("] ");
                    stringBuilder.append(String.format(log.getLog(), log.getArgs()));
                    return stringBuilder.toString();
                } catch (Exception exception) {
                    return log.getLog();
                }
            }

            @Override
            public void run() {
                while (true) {
                    try {
                        android.util.Log.d("LOGGER", "Taking log from queue");
                        Log poll = logs.take();
                        android.util.Log.d("LOGGER", "Polled a log");
                        String logLine = prepareLog(poll);
                        switch (poll.getLogLevel()) {
                            case INFO:
                                android.util.Log.i(poll.getTag(), logLine);
                                break;
                            case DEBUG:
                                android.util.Log.d(poll.getTag(), logLine);
                                break;
                            case ERROR:
                                android.util.Log.e(poll.getTag(), logLine, poll.getThrowable());
                                break;
                        }
                        writeToFile(logLine, poll.getThrowable());
                    } catch (InterruptedException e) {
                        //do nothing just continue
                    }
                }
            }
        });
    }

    private void writeToFile(String logLine, Throwable throwable) {
        try {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(logLine);
            buf.newLine();
            if (throwable != null) {
                throwable.printStackTrace(new PrintWriter(buf));
            }
            buf.close();
        } catch (IOException e) {
            //do nothing
            android.util.Log.d("LOGGER", "Failed to log into the file");
        }
    }

    public void submit(Log log) {
        android.util.Log.d("LOGGER", "Adding log to the queue");
        this.logs.add(log);
    }

}
