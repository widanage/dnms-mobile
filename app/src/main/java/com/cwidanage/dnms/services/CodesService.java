package com.cwidanage.dnms.services;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cwidanage.dnms.entity.Code;
import com.cwidanage.dnms.services.models.CodesResponse;
import com.cwidanage.dnms.services.requests.GsonRequest;
import com.cwidanage.dnms.util.logging.LogManager;
import com.cwidanage.dnms.util.logging.Logger;
import com.orm.SugarTransactionHelper;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Chathura Widanage
 */

public class CodesService extends AbstractService {

    private final Logger logger = LogManager.getLogger(CodesService.class);

    private final static String LIMIT_PARAM = "limit";

    private final static long codesBuffer = 1000;

    public final static CodesService INSTANCE = new CodesService();

    private Context context;

    private CodesService() {
    }

    public void returnCode(String code) {
        List<Code> codes = Code.find(Code.class, "CODE = ?", code);
        if (!codes.isEmpty()) {
            codes.get(0).setUsed(false);
            Code.update(code);
        }
    }

    public String burrowCode() {
        List<Code> codes = Code.find(Code.class, "USED = ? LIMIT 1", "0");
        if (codes.isEmpty()) {
            logger.error("No unused codes remaining in the stock. Online connection required before proceeding");
            throw new RuntimeException("No unused codes remaining in the stock. Online connection required before proceeding.");
        }
        Code code = codes.get(0);
        code.setUsed(true);
        Code.update(code);
        return code.getCode();
    }

    public long getAvailableCodesCount() {
        return Code.count(Code.class, "USED = ?", new String[]{"0"});
    }

    @Override
    public void syncFromServer(final SyncListener syncListener) {
        final long codesToFetch = Math.max(0, codesBuffer - getAvailableCodesCount());
        logger.debug("Fetching %l codes from server", codesToFetch);
        if (codesToFetch == 0) {
            syncListener.onDone("Nothing to sync in codes");
            return;
        }
        final GsonRequest<CodesResponse> codesRequest = new GsonRequest<>(
                getSyncEndpointUrl(Collections.singletonMap(LIMIT_PARAM, Long.toString(codesToFetch))),
                CodesResponse.class,
                headers,
                new Response.Listener<CodesResponse>() {
                    @Override
                    public void onResponse(final CodesResponse response) {
                        logger.debug("Response received for codes request");
                        SugarTransactionHelper.doInTransaction(new SugarTransactionHelper.Callback() {
                            @Override
                            public void manipulateInTransaction() {
                                for (String code : response.getCodes()) {
                                    Code c = new Code();
                                    c.setCode(code);
                                    Code.save(c);
                                }
                            }
                        });
                        syncListener.onDone("Fetched " + codesToFetch + " new unique codes");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.error("Error occurred while synchronizing codes", error);
                syncListener.onError(error);
            }
        });
        this.fireRequest(codesRequest);
    }

    @Override
    public String getSyncEndpointQueryString(Map<String, String> params) {
        StringBuilder stringBuilder = new StringBuilder("system/id.json?limit=").append(params.get(LIMIT_PARAM));
        return stringBuilder.toString();
    }
}
