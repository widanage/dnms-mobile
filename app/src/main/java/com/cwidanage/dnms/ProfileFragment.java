package com.cwidanage.dnms;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.cwidanage.dnms.constants.Gender;
import com.cwidanage.dnms.constants.ThriposhaStatus;
import com.cwidanage.dnms.constants.TrackedEntityAttribute;
import com.cwidanage.dnms.entity.OrganizationUnit;
import com.cwidanage.dnms.entity.TrackedEntityInstance;
import com.cwidanage.dnms.entity.TrackedEntityInstanceAttribute;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.services.OrganizationUnitService;
import com.cwidanage.dnms.services.TrackedEntityInstanceService;
import com.cwidanage.dnms.util.DateUtils;
import com.cwidanage.dnms.util.logging.LogManager;
import com.cwidanage.dnms.util.logging.Logger;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


/**
 * @author Chathura Widanage
 */
public class ProfileFragment extends Fragment {

    private final static Logger logger = LogManager.getLogger(ProfileFragment.class);

    private static final String TEI_ID = "TEI_ID";

    private String teiId;
    private TrackedEntityInstance trackedEntityInstance;

    private OnFragmentInteractionListener mListener;

    /*TEXT FIELDS*/
    private Spinner gnDivisionSpinner;
    private EditText chdrNumberTxt;
    private EditText firstNameTxt;
    private EditText otherNamesTxt;
    private EditText lastNameTxt;
    private EditText dateOfBirth;
    private EditText birthWeightTxt;
    private Spinner genderSpinner;

    private EditText guardianNameTxt;
    private EditText guardianNic;
    private EditText guardianPhone;

    private EditText phnNumber;
    private EditText familyNumber;
    private Spinner thriposhaSpinner;

    private ArrayAdapter<String> genderAdapter;
    private ArrayAdapter<String> thriposhaAdapter;
    private ArrayAdapter<String> gnDivisionAdapter;

    private List<OrganizationUnit> organizationUnitList;

    private boolean initialized;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(String teiId) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(TEI_ID, teiId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*Gender spinner*/
        List<String> genderList = new ArrayList<>();
        for (Gender gender : Gender.values()) {
            genderList.add(getString(gender.getStringId()));
        }
        this.genderAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, genderList);

        /*Thriposha Spinner*/
        List<String> thriposhaStatusList = new ArrayList<>();
        for (ThriposhaStatus thriposhaStatus : ThriposhaStatus.values()) {
            thriposhaStatusList.add(getString(thriposhaStatus.getStringId()));
        }
        this.thriposhaAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, thriposhaStatusList);

        /*OU Spinner*/
        List<String> gnDivisionsList = new ArrayList<>();
        gnDivisionsList.add(getString(R.string.select));
        this.organizationUnitList = new ArrayList<>();
        this.organizationUnitList.add(null);
        Iterator<OrganizationUnit> allOus = OrganizationUnitService.INSTANCE.getAll();
        while (allOus.hasNext()) {
            OrganizationUnit next = allOus.next();
            this.organizationUnitList.add(next);
            gnDivisionsList.add(next.getDisplayName());
        }
        this.gnDivisionAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, gnDivisionsList);

        if (getArguments() != null) {
            this.teiId = getArguments().getString(TEI_ID);
            if (teiId != null) {
                try {
                    this.trackedEntityInstance = TrackedEntityInstanceService.INSTANCE.getById(this.teiId);
                    this.trackedEntityInstance.loadRelations();
                } catch (NotFoundInDBException e) {
                    e.printStackTrace();
                }
            } else {//new

            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //specially handle gn
        this.gnDivisionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                OrganizationUnit organizationUnit = organizationUnitList.get(position);
                if (organizationUnit == null) {
                    trackedEntityInstance.setOrgUnit(null);
                    trackedEntityInstance.setOrganizationUnit(null);
                } else {
                    trackedEntityInstance.setOrgUnit(organizationUnit.getOuId());
                    trackedEntityInstance.setOrganizationUnit(organizationUnit);
                    trackedEntityInstance.setShouldSync(true);
                }
                mListener.onHeaderFieldUpdated();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        String orgUnit = trackedEntityInstance.getOrgUnit();
        int index = 0;
        for (int i = 1; i < this.organizationUnitList.size(); i++) {
            if (this.organizationUnitList.get(i).getOuId().equals(orgUnit)) {
                index = i;
                break;
            }
        }
        this.gnDivisionSpinner.setSelection(index);

        //handling others

        this.bindEditTextWithTeiAttribute(chdrNumberTxt, TrackedEntityAttribute.CHDR_NUMBER);
        this.bindEditTextWithTeiAttribute(firstNameTxt, TrackedEntityAttribute.FIRST_NAME);
        this.bindEditTextWithTeiAttribute(otherNamesTxt, TrackedEntityAttribute.OTHER_NAMES);
        this.bindEditTextWithTeiAttribute(lastNameTxt, TrackedEntityAttribute.LAST_NAME);
        this.bindEditTextWithTeiAttribute(dateOfBirth, TrackedEntityAttribute.DOB);

        //gender
        List<String> genderValues = new ArrayList<>();
        for (Gender gender : Gender.values()) {
            genderValues.add(gender.getValue());
        }
        this.bindSpinnerWithTeiAttribute(genderSpinner, TrackedEntityAttribute.GENDER, genderValues);

        this.bindEditTextWithTeiAttribute(birthWeightTxt, TrackedEntityAttribute.BIRTH_WEIGHT);

        this.bindEditTextWithTeiAttribute(guardianNameTxt, TrackedEntityAttribute.NAME_OF_GUARDIAN);
        //this.bindEditTextWithTeiAttribute(guardianPhone,);
        this.bindEditTextWithTeiAttribute(guardianNic, TrackedEntityAttribute.NIC);

        this.bindEditTextWithTeiAttribute(phnNumber, TrackedEntityAttribute.PHN);
        this.bindEditTextWithTeiAttribute(familyNumber, TrackedEntityAttribute.ELIGIBILE_FAMILY_NUMBER);

        //thriposha
        List<String> thriposhaValues = new ArrayList<>();
        for (ThriposhaStatus thriposhaStatus : ThriposhaStatus.values()) {
            thriposhaValues.add(thriposhaStatus.getValue());
        }
        this.bindSpinnerWithTeiAttribute(this.thriposhaSpinner, TrackedEntityAttribute.THRIPOSHA_STATUS, thriposhaValues);

        this.trackedEntityInstance.reloadAttributesMap();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teiprofile, container, false);

        this.gnDivisionSpinner = (Spinner) view.findViewById(R.id.teiOrgUnitSpinner);
        this.gnDivisionSpinner.setAdapter(this.gnDivisionAdapter);

        this.chdrNumberTxt = (EditText) view.findViewById(R.id.teiCHDRTxt);
        this.firstNameTxt = (EditText) view.findViewById(R.id.teiFirstNameTxt);
        this.otherNamesTxt = (EditText) view.findViewById(R.id.teiOtherNameTxt);
        this.lastNameTxt = (EditText) view.findViewById(R.id.teiLastNameTxt);
        this.dateOfBirth = (EditText) view.findViewById(R.id.teiDobTxt);
        this.dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                try {
                    String dobString = dateOfBirth.getText().toString();
                    if (!dobString.trim().isEmpty()) {
                        Date parsedDate = DateUtils.parseToStandardDate(dobString);
                        calendar.setTime(parsedDate);
                    }
                } catch (ParseException e) {
                    logger.error( "Error in parsing date", e);
                    //ignore
                }
                new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String dateString = DateUtils.formatFromStandardDate(calendar.getTime());
                        dateOfBirth.setText(dateString);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        this.birthWeightTxt = (EditText) view.findViewById(R.id.teiBirthWeightTxt);
        this.genderSpinner = (Spinner) view.findViewById(R.id.teiGenderSpinner);
        this.genderSpinner.setAdapter(this.genderAdapter);

        this.guardianNameTxt = (EditText) view.findViewById(R.id.teiGuardianNameTxt);
        this.guardianPhone = (EditText) view.findViewById(R.id.teiGuardianPhoneTxt);
        this.guardianNic = (EditText) view.findViewById(R.id.teiGuardianNICTxt);

        this.phnNumber = (EditText) view.findViewById(R.id.teiPHNTxt);
        this.familyNumber = (EditText) view.findViewById(R.id.teiFamilyNumberTxt);

        this.thriposhaSpinner = (Spinner) view.findViewById(R.id.teiThriposhaSpinner);
        this.thriposhaSpinner.setAdapter(this.thriposhaAdapter);

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        this.initialized = false;
    }

    @Override
    public void onStart() {
        super.onStart();
        this.initialized = true;
    }

    private TrackedEntityInstanceAttribute getBoundedAttributed(TrackedEntityAttribute teiAttr) {
        if (!this.trackedEntityInstance
                .getAttributesMap().containsKey(teiAttr.getId())) {
            TrackedEntityInstanceAttribute trackedEntityInstanceAttribute = new TrackedEntityInstanceAttribute();
            trackedEntityInstanceAttribute.setTeiId(this.trackedEntityInstance.getTrackedEntityInstance());
            trackedEntityInstanceAttribute.setAttribute(teiAttr.getId());
            trackedEntityInstanceAttribute.setFresh(true);
            this.trackedEntityInstance.getAttributes().add(trackedEntityInstanceAttribute);
            return trackedEntityInstanceAttribute;
        }

        return this.trackedEntityInstance.getAttributesMap().get(teiAttr.getId());
    }

    private void bindSpinnerWithTeiAttribute(Spinner spinner, final TrackedEntityAttribute teiAttr, final List<String> values) {
        if (this.trackedEntityInstance == null || spinner == null) {
            return;
        }

        final TrackedEntityInstanceAttribute trackedEntityInstanceAttribute = getBoundedAttributed(teiAttr);
        if (trackedEntityInstanceAttribute.getValue() != null) {
            spinner.setSelection(values.indexOf(trackedEntityInstanceAttribute.getValue()));
        } else {
            spinner.setSelection(0);
            trackedEntityInstanceAttribute.setValue(values.get(0));
            trackedEntityInstanceAttribute.setShouldSync(true);
        }

        final int initialSelection = spinner.getSelectedItemPosition();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                trackedEntityInstanceAttribute.setValue(values.get(position));
                if (teiAttr.equals(TrackedEntityAttribute.GENDER)) {
                    mListener.onHeaderFieldUpdated();
                }

                if (position != initialSelection) {
                    markAsPropertyChanged(trackedEntityInstanceAttribute);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void markAsPropertyChanged(TrackedEntityInstanceAttribute trackedEntityInstanceAttribute) {
        if (initialized) {
            trackedEntityInstanceAttribute.setLastUpdated(Calendar.getInstance().getTime());
            trackedEntityInstanceAttribute.setShouldSync(true);
            mListener.onPropertyChanged();
        }
    }

    private void bindEditTextWithTeiAttribute(EditText editText, final TrackedEntityAttribute teiAttr) {
        if (this.trackedEntityInstance == null || editText == null) {
            return;
        }

        final TrackedEntityInstanceAttribute trackedEntityInstanceAttribute = getBoundedAttributed(teiAttr);
        if (trackedEntityInstanceAttribute.getValue() != null) {
            editText.setText(trackedEntityInstanceAttribute.getValue());
        }

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                trackedEntityInstanceAttribute.setValue(s.toString());
                markAsPropertyChanged(trackedEntityInstanceAttribute);
                if (teiAttr.equals(TrackedEntityAttribute.OTHER_NAMES)
                        || teiAttr.equals(TrackedEntityAttribute.FIRST_NAME)
                        || teiAttr.equals(TrackedEntityAttribute.LAST_NAME)
                        || teiAttr.equals(TrackedEntityAttribute.DOB)) {
                    mListener.onHeaderFieldUpdated();
                }
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onHeaderFieldUpdated();

        void onPropertyChanged();
    }
}
