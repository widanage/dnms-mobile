package com.cwidanage.dnms.constants;

import com.cwidanage.dnms.R;

/**
 * @author Chathura
 */

public enum SDCategory {

    LS_TN_MIN_3("Less than -3SD", "Less than -3SD", R.color.ls_tn_min_3),
    BTW_MIN_2_MIN_3("Between -2SD to -3SD", "Between -2SD to -3SD", R.color.btw_min_2_min_3),
    BTW_MIN_1_MIN_2("Between -1SD to -2SD", "Between -1SD to -2SD", R.color.btw_min_1_min_2),
    BTW_MIN_1_2("Between -1SD to +2SD", "Between -1SD to +2SD", R.color.btw_min_1_2),
    MR_2("More than 2 SD", "More than +2 SD", R.color.MR_2);

    private String value;
    private String displayName;
    private int color;

    SDCategory(String value, String display, int color) {
        this.value = value;
        this.displayName = display;
        this.color = color;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
