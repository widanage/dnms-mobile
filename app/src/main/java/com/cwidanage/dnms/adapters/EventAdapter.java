package com.cwidanage.dnms.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cwidanage.dnms.TrackedEntityInstanceActivity;
import com.cwidanage.dnms.constants.DataElement;
import com.cwidanage.dnms.R;
import com.cwidanage.dnms.entity.DataValue;
import com.cwidanage.dnms.entity.Event;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.listeners.EventEditRequestListener;
import com.cwidanage.dnms.services.InstanceSpecificIDs;
import com.cwidanage.dnms.util.logging.LogManager;
import com.cwidanage.dnms.util.logging.Logger;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Chathura Widanage
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventViewHolder> {

    private final static Logger logger = LogManager.getLogger(EventAdapter.class);
    private final EventEditRequestListener eventEditRquestListener;

    private List<Event> eventList;
    private Context context;

    public EventAdapter(Context context, List<Event> eventList, EventEditRequestListener eventEditRequestListener) {
        this.eventList = eventList;
        this.context = context;
        this.eventEditRquestListener = eventEditRequestListener;
    }

    public List<Event> getEventList() {
        return eventList;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_card, parent, false);
        return new EventViewHolder(itemView);
    }

    private void handleDeleteUi(boolean delete, EventViewHolder holder) {
        if (delete) {
            holder.itemView.setAlpha(0.2f);
            holder.deleteBtn.setText(R.string.undele);
        } else {
            holder.itemView.setAlpha(1f);
            holder.deleteBtn.setText(R.string.delete);
        }
    }

    @Override
    public void onBindViewHolder(final EventViewHolder holder, final int position) {
        final Event event = this.eventList.get(position);

        //reduce opacity if marked for delete
        handleDeleteUi(event.isMarkForDelete(), holder);

        switch (event.getProgramStage()) {
            case InstanceSpecificIDs.PROGRAM_STAGE_NUT_MONITORING:
                holder.eventTitle.setText(R.string.nutrition_monitoring);
                break;
            case InstanceSpecificIDs.PROGRAM_STAGE_RISK_ID:
                holder.eventTitle.setText(R.string.risk_identification);
                break;
            case InstanceSpecificIDs.PROGRAM_STAGE_REGISTRATION:
                holder.eventTitle.setText(R.string.registration);
                break;
            default:
                holder.eventTitle.setText(R.string.unknown_event);
        }

        Date eventDate = event.getEventDate();
        try {
            event.loadRelations();
        } catch (NotFoundInDBException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(eventDate);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        holder.dateDay.setText(String.valueOf(day));
        String[] shortMonths = context.getResources().getStringArray(R.array.short_months);
        holder.dateMonth.setText(shortMonths[month]);
        holder.dateYear.setText(String.valueOf(year));

        holder.eventPreviewList.removeAllViews();
        for (DataValue dataValue : event.getDataValues()) {
            if (event.getProgramStage().equals(InstanceSpecificIDs.PROGRAM_STAGE_RISK_ID)
                    && (dataValue.getValue() == null || dataValue.getValue().equals("false"))) {//skip false values
                continue;
            }
            View dataValueView = LayoutInflater.from(this.context)
                    .inflate(R.layout.event_preview_list_item, null, false);
            TextView value = (TextView) dataValueView.findViewById(R.id.data_value);
            TextView valueName = (TextView) dataValueView.findViewById(R.id.data_value_name);
            ImageView image = (ImageView) dataValueView.findViewById(R.id.data_value_icon);

            DataElement dataElement = InstanceSpecificIDs.dataElementMap.get(dataValue.getDataElement());
            if (dataElement.getIconId() != -1) {
                image.setImageResource(dataElement.getIconId());
            }
            //resolving icon

            value.setText(dataValue.getValue());
            valueName.setText(context.getString(dataElement.getStringId()));
            holder.eventPreviewList.addView(dataValueView);
        }

        //handle delete
        holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(event.isMarkForDelete() ? R.string.event_undelete : R.string.event_delete));
                alertDialog.setMessage(context.getString(event.isMarkForDelete() ? R.string.event_undelete_prompt : R.string.event_delete_prompt));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!event.isMarkForDelete()) {//delete mode
                            event.setMarkForDelete(true);
                            if (event.isFresh()) {
                                eventList.remove(position);
                                notifyItemRemoved(position);
                            } else {
                                handleDeleteUi(true, holder);
                            }
                        } else {
                            event.setMarkForDelete(false);
                            handleDeleteUi(false, holder);
                        }
                    }
                });

                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, context.getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        // holder.eventPreviewList.setAdapter(new EventDataValueListAdapter(context, event.getDataValues()));

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventEditRquestListener.onEditEventRequest(event, position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return this.eventList.size();
    }


    public class EventViewHolder extends RecyclerView.ViewHolder {

        private TextView eventTitle;
        private TextView dateDay;
        private TextView dateMonth;
        private TextView dateYear;
        private LinearLayout eventPreviewList;

        private ImageButton viewBtn;
        private Button deleteBtn;
        private Button editButton;

        private boolean previewVisible = false;

        private View itemView;

        public EventViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            eventTitle = (TextView) itemView.findViewById(R.id.event_title);
            dateDay = (TextView) itemView.findViewById(R.id.event_day);
            dateMonth = (TextView) itemView.findViewById(R.id.event_month);
            dateYear = (TextView) itemView.findViewById(R.id.event_year);
            eventPreviewList = (LinearLayout) itemView.findViewById(R.id.event_previews_list);

            viewBtn = (ImageButton) itemView.findViewById(R.id.view_btn);
            viewBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (previewVisible) {
                        eventPreviewList.setVisibility(View.GONE);
                        viewBtn.setImageResource(R.drawable.ic_expand_more_black_24dp);
                    } else {
                        eventPreviewList.setVisibility(View.VISIBLE);
                        viewBtn.setImageResource(R.drawable.ic_expand_less_black_24dp);
                    }
                    previewVisible = !previewVisible;
                }
            });

            deleteBtn = (Button) itemView.findViewById(R.id.delete_btn);
            editButton = (Button) itemView.findViewById(R.id.edit_btn);
        }
    }
}
