package com.cwidanage.dnms.services.models;

import com.cwidanage.dnms.entity.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Chathura Widanage
 */

public class EventResponse {

    private List<Event> events = new ArrayList<>();

    private Pager pager;

    public Pager getPager() {
        return pager;
    }

    public void setPager(Pager pager) {
        this.pager = pager;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }
}
