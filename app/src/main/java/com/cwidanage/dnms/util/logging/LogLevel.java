package com.cwidanage.dnms.util.logging;

/**
 * @author Chathura Widanage
 */

public enum LogLevel {
    INFO, DEBUG, ERROR, TRACE
}
