package com.cwidanage.dnms;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cwidanage.dnms.adapters.EventAdapter;
import com.cwidanage.dnms.constants.Event;
import com.cwidanage.dnms.entity.TrackedEntityInstance;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.listeners.EventCreationListener;
import com.cwidanage.dnms.listeners.EventEditRequestListener;
import com.cwidanage.dnms.services.TrackedEntityInstanceService;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;


/**
 * @author Chathura Widanage
 */
public class EventsFragment extends Fragment implements EventCreationListener, EventEditRequestListener {

    private static final String TEI_ID = "TEI_ID";

    private String teiId;

    private RecyclerView eventsRecycleView;
    private EventAdapter eventAdapter;

    private FloatingActionButton newRegistrationEventBtn;
    private FloatingActionButton newNutritionMonitoringEventBtn;
    private FloatingActionButton newRiskIdentificationEventBtn;

    private OnFragmentInteractionListener mListener;
    private TrackedEntityInstance trackedEntityInstance;

    public EventsFragment() {
        // Required empty public constructor
    }

    public static EventsFragment newInstance(String trackedEntityInstanceId) {
        EventsFragment fragment = new EventsFragment();
        Bundle args = new Bundle();
        args.putString(TEI_ID, trackedEntityInstanceId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            teiId = getArguments().getString(TEI_ID);
            if (teiId != null) {
                try {
                    this.trackedEntityInstance = TrackedEntityInstanceService.INSTANCE.getById(this.teiId);
                    this.trackedEntityInstance.loadRelations();
                } catch (NotFoundInDBException e) {
                    e.printStackTrace();
                }
            } else {//new

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teievents, container, false);
        this.eventsRecycleView = (RecyclerView) view.findViewById(R.id.events_list);

        final FloatingActionsMenu floatingActionsMenu = (FloatingActionsMenu) view.findViewById(R.id.right_labels);

        this.newRegistrationEventBtn = (FloatingActionButton) view.findViewById(R.id.new_reg_event_btn);
        this.newRegistrationEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegistrationEventFragment registrationEventFragment = RegistrationEventFragment.newInstance(teiId, EventsFragment.this);
                registrationEventFragment.show(getFragmentManager(), "reg-dialog");
                floatingActionsMenu.collapse();
            }
        });

        this.newNutritionMonitoringEventBtn = (FloatingActionButton) view.findViewById(R.id.new_nut_monitoring_event_btn);
        this.newNutritionMonitoringEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NutritionMonitoringEventFragment nutritionMonitoringEventFragment = NutritionMonitoringEventFragment.newInstance(teiId, EventsFragment.this);
                nutritionMonitoringEventFragment.show(getFragmentManager(), "nut-dialog");
                floatingActionsMenu.collapse();
            }
        });

        this.newRiskIdentificationEventBtn = (FloatingActionButton) view.findViewById(R.id.new_risk_id_event_btn);
        this.newRiskIdentificationEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RiskIdentificationEventFragment riskIdentificationEventFragment = RiskIdentificationEventFragment.newInstance(teiId, EventsFragment.this);
                riskIdentificationEventFragment.show(getFragmentManager(), "risk-dialog");
                floatingActionsMenu.collapse();

            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        this.eventsRecycleView.setLayoutManager(mLayoutManager);
        this.eventsRecycleView.setItemAnimator(new DefaultItemAnimator());
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.eventAdapter = new EventAdapter(getContext(),
                this.trackedEntityInstance.getEvents(), this);
        this.eventsRecycleView.setAdapter(eventAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onEventCreated(com.cwidanage.dnms.entity.Event event, final boolean newEvent, final int eventPosition) {
        if (newEvent) {
            this.eventAdapter.getEventList().add(0, event);
        } else {
            this.eventAdapter.getEventList().set(eventPosition, event);
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (newEvent) {
                    eventAdapter.notifyItemInserted(0);
                } else {
                    eventAdapter.notifyItemChanged(eventPosition);
                }
            }
        });
    }

    @Override
    public void onEditEventRequest(com.cwidanage.dnms.entity.Event event, int position) {
        if (event.getProgramStage().equals(Event.REGISTRATION.getId())) {
            RegistrationEventFragment registrationEventFragment =
                    RegistrationEventFragment.newInstance(
                            teiId, event.getId(), position, EventsFragment.this
                    );
            registrationEventFragment.show(getFragmentManager(), "reg-dialog");
        } else if (event.getProgramStage().equals(Event.NUTRITION_MONITORING.getId())) {
            NutritionMonitoringEventFragment nutritionMonitoringEventFragment =
                    NutritionMonitoringEventFragment.newInstance(
                            teiId, event.getId(), position, EventsFragment.this
                    );
            nutritionMonitoringEventFragment.show(getFragmentManager(), "nut-dialog");
        }
    }

    public interface OnFragmentInteractionListener {
        void onCreateNewEvent(Event event);
    }
}
