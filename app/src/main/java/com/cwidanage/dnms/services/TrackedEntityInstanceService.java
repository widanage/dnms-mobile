package com.cwidanage.dnms.services;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cwidanage.dnms.constants.TrackedEntityAttribute;
import com.cwidanage.dnms.entity.OrganizationUnit;
import com.cwidanage.dnms.entity.TrackedEntityInstance;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.services.models.TrackedEntityInstanceResponse;
import com.cwidanage.dnms.services.requests.JsonObjectPostRequest;
import com.cwidanage.dnms.services.requests.GsonRequest;
import com.cwidanage.dnms.services.requests.VolleyResponseListener;
import com.cwidanage.dnms.util.ResponseTracker;
import com.cwidanage.dnms.util.gson.SerializationExclusionStratergy;
import com.cwidanage.dnms.util.logging.LogManager;
import com.cwidanage.dnms.util.logging.Logger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.SugarRecord;
import com.orm.SugarTransactionHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.SyncFailedException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Chathura Widanage
 */

public class TrackedEntityInstanceService extends AbstractService {

    private final static Logger logger = LogManager.getLogger(TrackedEntityInstanceService.class);

    public final static TrackedEntityInstanceService INSTANCE = new TrackedEntityInstanceService();

    private OrganizationUnitService ouService = OrganizationUnitService.INSTANCE;

    private TrackedEntityInstanceService() {

    }

    @Override
    public void syncFromServer(final SyncListener syncListener) {
        this.syncPageFromServer(1, syncListener);
    }

    @Override
    public void syncToServer(final SyncListener syncListener) {
        //new TEIs
        List<TrackedEntityInstance> trackedEntityInstances = SugarRecord.find(TrackedEntityInstance.class, "FRESH = ? OR SHOULD_SYNC = ?", "1", "1");

        logger.debug("Starting to sync %d TEIs to server", trackedEntityInstances.size());

        if (trackedEntityInstances.isEmpty()) {
            syncListener.onDone("Nothing to sync in Tracked Entity Instances");
        }

        final ResponseTracker responseTracker = new ResponseTracker(trackedEntityInstances.size());
        responseTracker.setOnDoneListener(new ResponseTracker.OnDoneListener() {
            @Override
            public void onDone(int total, int failed) {
                logger.info("%d out of %d TEIs failed to synchronize", failed, total);
                if (failed != 0) {
                    syncListener.onError(new SyncFailedException(failed + " out of " + total + " TEIs failed to synchronize."));
                } else {
                    syncListener.onDone(total + " TEIs synchronized with server");
                }
            }
        });

        Gson gson = new GsonBuilder().addSerializationExclusionStrategy(new SerializationExclusionStratergy())
                .create();

        //can send all at once to the server, but hard to track failures etc.
        for (final TrackedEntityInstance trackedEntityInstance : trackedEntityInstances) {
            JSONObject teiJson = null;
            try {
                trackedEntityInstance.loadRelations();
                teiJson = new JSONObject(gson.toJson(trackedEntityInstance));
            } catch (JSONException e) {
                responseTracker.requestFailed();
                logger.error("Error in converting tei %s to json", e, trackedEntityInstance.getTrackedEntityInstance());
                continue;
            } catch (NotFoundInDBException e) {
                logger.error("Failed to load relations of %s", e, trackedEntityInstance.getTrackedEntityInstance());
                syncListener.onError(e);
                continue;
            }

            JsonObjectPostRequest teiPostRequest = new JsonObjectPostRequest(
                    getPostEndpointUrl(), teiJson, new VolleyResponseListener<JSONObject>() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    responseTracker.requestFailed();
                    logger.error("Error occurred while posting tei %d", error, trackedEntityInstance.getTrackedEntityInstance());
                    //do nothing
                }

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        int responseCode = response.getInt("httpStatusCode");
                        if (responseCode == 200) {
                            SugarTransactionHelper.doInTransaction(new SugarTransactionHelper.Callback() {
                                @Override
                                public void manipulateInTransaction() {
                                    logger.debug("Marking TEI %s as synced", trackedEntityInstance.getTrackedEntityInstance());
                                    trackedEntityInstance.setSynced();
                                    responseTracker.responseReceived();
                                }
                            });
                        } else {
                            logger.error("Unexpected response code %d received for TEI %s. Response : %s",
                                    responseCode,
                                    trackedEntityInstance.getTrackedEntityInstance(), response.toString());
                            responseTracker.requestFailed();
                        }
                    } catch (JSONException e) {
                        logger.error("Unexpected response received for TEI POST request", e, response.toString());
                        responseTracker.requestFailed();
                    }
                }
            }, headers);

            fireRequest(teiPostRequest);
        }
    }

    public void clearTemp() {
        //todo return borrowed codes
        TrackedEntityInstance.deleteAll(TrackedEntityInstance.class, "TEMPORARY = 1");
    }


    private ResponseTracker responseTracker;

    private void syncPageFromServer(final int page, final SyncListener syncListener) {
        logger.debug("Sending request to fetch page %d of TEIS", page);
        syncListener.onProgress(String.format("Starting to synchronizing set %d of children", page));
        final GsonRequest<TrackedEntityInstanceResponse> request
                = new GsonRequest<>(
                getSyncEndpointUrl(Collections.singletonMap(PAGE_PARAM, Integer.toString(page))),
                TrackedEntityInstanceResponse.class,
                headers, new Response.Listener<TrackedEntityInstanceResponse>() {
            @Override
            public void onResponse(final TrackedEntityInstanceResponse response) {
                logger.debug("Tei response received for page %d", page);
                syncListener.onProgress(String.format("Data received for set %d of children", page));
                //send request for other pages
                if (response.getPager().getPage() == 1) {
                    responseTracker = new ResponseTracker(response.getPager().getPageCount());
                    for (int i = 1; i < response.getPager().getPageCount(); i++) {
                        syncPageFromServer(i + 1, syncListener);
                    }
                }


                final List<TrackedEntityInstance> trackedEntityInstances = response.getTrackedEntityInstances();

                SugarTransactionHelper.doInTransaction(new SugarTransactionHelper.Callback() {
                    @Override
                    public void manipulateInTransaction() {
                        try {
                            for (TrackedEntityInstance trackedEntityInstance : trackedEntityInstances) {
                                trackedEntityInstance.saveToDb(true);
                            }
                            if (responseTracker.responseReceived()) {
                                syncListener.onDone(null);
                            }
                        } catch (Exception e) {
                            logger.error("Failed to save tracked entities of page %d in the transaction", e, page);
                            syncListener.onError(e);
                        }
                    }
                });


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.error("Rest call failed for page %d", error, page);
                syncListener.onError(error);
            }
        });
        logger.debug("Started syncing TEIs for page %d", page);
        this.fireRequest(request);
    }

    //caching last access so it will be shared among all activities and fragments
    private TrackedEntityInstance lastAccess;

    public void clearLastAccess() {
        this.lastAccess = null;
    }

    public TrackedEntityInstance getById(String teiId) throws NotFoundInDBException {
        if (this.lastAccess != null && this.lastAccess.getTrackedEntityInstance().equals(teiId)) {
            return lastAccess;
        }

        List<TrackedEntityInstance> trackedEntityInstances = SugarRecord.find(TrackedEntityInstance.class, "TRACKED_ENTITY_INSTANCE = ? ", teiId);
        if (trackedEntityInstances.isEmpty()) {
            throw new NotFoundInDBException("Couldn't find a tracked entity instance with id " + teiId);
        }
        TrackedEntityInstance trackedEntityInstance = trackedEntityInstances.get(0);
        trackedEntityInstance.loadRelations();
        this.lastAccess = trackedEntityInstances.get(0);
        return lastAccess;
    }

    public List<TrackedEntityInstance> search(String keyword, Set<String> orgUnits) {
        /*StringBuilder orgUnitsQuery = new StringBuilder();
        for (int i = 0; i < orgUnits.size(); i++) {
            orgUnitsQuery.append("'");
            orgUnitsQuery.append(orgUnits.get(i));
            orgUnitsQuery.append("'");
            if (i != orgUnits.size() - 1) {
                orgUnitsQuery.append(",");
            }
        }*/

//AND `ORG_UNIT` IN (?)
        List<TrackedEntityInstance> withQuery = SugarRecord.findWithQuery(TrackedEntityInstance.class,
                "SELECT * FROM `TRACKED_ENTITY_INSTANCE` WHERE TRACKED_ENTITY_INSTANCE IN " +
                        "(SELECT `TEI_ID` FROM `TRACKED_ENTITY_INSTANCE_ATTRIBUTE` " +
                        "WHERE `ATTRIBUTE` IN (?,?,?,?) AND `VALUE` LIKE ?)",
                TrackedEntityAttribute.CHDR_NUMBER.getId(),
                TrackedEntityAttribute.FIRST_NAME.getId(),
                TrackedEntityAttribute.OTHER_NAMES.getId(),
                TrackedEntityAttribute.LAST_NAME.getId(),
                "%" + keyword + "%");

        Iterator<TrackedEntityInstance> iterator = withQuery.iterator();
        while (iterator.hasNext()) {
            TrackedEntityInstance next = iterator.next();
            if (!orgUnits.contains(next.getOrgUnit())) {
                iterator.remove();
            }
        }
        return withQuery;
    }

    @Override
    public String getPostEndpointUrl() {
        return this.baseUrl + "trackedEntityInstances.json?strategy=CREATE_AND_UPDATE";
    }

    @Override
    public String getSyncEndpointQueryString(Map<String, String> params) {
        StringBuilder syncEndpointBuilder = new StringBuilder("trackedEntityInstances.json?ou=");
        Iterator<OrganizationUnit> allOusIterator = ouService.getAll();
        while (allOusIterator.hasNext()) {
            syncEndpointBuilder.append(allOusIterator.next().getOuId());
            syncEndpointBuilder.append(';');
        }

        if (params.containsKey(PAGE_PARAM)) {
            syncEndpointBuilder.append("&page=" + params.get(PAGE_PARAM));
        }

        syncEndpointBuilder.append("&totalPages=true&pageSize=20&fields=trackedEntityInstance,orgUnit,lastUpdated,attributes[lastUpdated,attribute,value]");
        return syncEndpointBuilder.toString();
    }
}
