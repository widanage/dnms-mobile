package com.cwidanage.dnms.services;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import org.junit.Test;

/**
 * @author Chathura Widanage
 */
public class TrackedEntityInstanceServiceTest {
    @Test
    public void sync() throws Exception {
        Context context = InstrumentationRegistry.getContext();
        TrackedEntityInstanceService trackedEntityInstanceService = TrackedEntityInstanceService.INSTANCE;
        trackedEntityInstanceService.syncFromServer(new SyncListener() {
            @Override
            public void onDone(String msg) {

            }

            @Override
            public void onProgress(String msg) {

            }

            @Override
            public void onError(Exception e) {

            }
        });
    }
}