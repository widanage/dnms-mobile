package com.cwidanage.dnms.services;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cwidanage.dnms.entity.Event;
import com.cwidanage.dnms.entity.OrganizationUnit;
import com.cwidanage.dnms.entity.Setting;
import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.services.models.EventResponse;
import com.cwidanage.dnms.services.models.Pager;
import com.cwidanage.dnms.services.requests.DeleteRequest;
import com.cwidanage.dnms.services.requests.JsonObjectPostRequest;
import com.cwidanage.dnms.services.requests.GsonRequest;
import com.cwidanage.dnms.services.requests.VolleyResponseListener;
import com.cwidanage.dnms.util.DateUtils;
import com.cwidanage.dnms.util.ResponseTracker;
import com.cwidanage.dnms.util.gson.SerializationExclusionStratergy;
import com.cwidanage.dnms.util.logging.LogManager;
import com.cwidanage.dnms.util.logging.Logger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.orm.SugarTransactionHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.SyncFailedException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Chathura Widanage
 */

public class EventService extends AbstractService {

    private final static Logger logger = LogManager.getLogger(EventService.class);

    public final static EventService INSTANCE = new EventService();

    public final static String KEY_OU = "OU";
    public final static String KEY_LAST_UPDATED = "LAST_UPDATED";

    private final static String SETTING_EVENTS_LAST_SYNCED_PREFIX = "SETTING_EVENTS_LAST_SYNCED_PREFIX_";

    private SettingService settingService = SettingService.INSTANCE;
    private OrganizationUnitService organizationUnitService = OrganizationUnitService.INSTANCE;

    private EventService() {

    }

    @Override
    public void syncToServer(final SyncListener syncListener) {
        List<Event> eventsToSync = Event.find(Event.class, "FRESH = ? OR SHOULD_SYNC = ?", "1", "1");
        List<Event> eventsToDelete = Event.find(Event.class, "MARK_FOR_DELETE = ?", "1");

        logger.debug("Starting to sync %d events and delete %d events", eventsToSync, eventsToDelete.size());

        if (eventsToSync.isEmpty() && eventsToDelete.isEmpty()) {
            syncListener.onDone("Nothing to sync in events");
            return;
        }

        final ResponseTracker responseTracker = new ResponseTracker(eventsToSync.size() + eventsToDelete.size());
        responseTracker.setOnDoneListener(new ResponseTracker.OnDoneListener() {
            @Override
            public void onDone(int total, int failed) {
                logger.info("%d out of %d events failed to synchronize", failed, total);
                if (failed != 0) {
                    syncListener.onError(new SyncFailedException(failed + " out of " + total + " events failed to synchronize."));
                } else {
                    syncListener.onDone(total + " events synchronized with server");
                }
            }
        });

        Gson gson = new GsonBuilder().addSerializationExclusionStrategy(new SerializationExclusionStratergy())
                .setDateFormat(DateUtils.getStandardDateFormat().toPattern())
                .create();

        for (final Event event : eventsToSync) {
            try {
                event.loadRelations();
                JSONObject jsonObject = new JSONObject(gson.toJson(event));
                JsonObjectPostRequest request = new JsonObjectPostRequest(
                        getPostEndpointUrl(),
                        jsonObject,
                        new VolleyResponseListener<JSONObject>() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                logger.error("Error in syncing event %s withs server", error, event.getEvent());
                                responseTracker.requestFailed();
                            }

                            @Override
                            public void onResponse(final JSONObject response) {
                                logger.debug("Sync Event response received : %s", response.toString());
                                SugarTransactionHelper.doInTransaction(new SugarTransactionHelper.Callback() {
                                    @Override
                                    public void manipulateInTransaction() {
                                        event.setSynced();
                                        responseTracker.responseReceived();
                                    }
                                });
                            }
                        },
                        headers
                );
                fireRequest(request);
            } catch (JSONException e) {
                responseTracker.requestFailed();
                logger.error("Error in converting event %s to json", e, event.getEvent());
            } catch (NotFoundInDBException e) {
                responseTracker.requestFailed();
                logger.error("Error in loading relations of event %s", e, event.getEvent());
            }
        }

        for (final Event event : eventsToDelete) {
            DeleteRequest deleteRequest = new DeleteRequest(getDeleteEndpointUrl(event.getEvent())
                    , new VolleyResponseListener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    SugarTransactionHelper.doInTransaction(new SugarTransactionHelper.Callback() {
                        @Override
                        public void manipulateInTransaction() {
                            logger.debug("Successfully deleted event %s from server", event.getEvent());
                            event.delete();
                            responseTracker.responseReceived();
                        }
                    });
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.error("Error occurred when deleting event in server %s", event.getEvent());
                    responseTracker.requestFailed();
                }
            }, headers);
            fireRequest(deleteRequest);
        }

        //todo handle delete
    }

    @Override
    public void syncFromServer(final SyncListener syncListener) {
        Iterator<OrganizationUnit> allOus = organizationUnitService.getAll();
        RequestContext requestContext = new RequestContext();
        while (allOus.hasNext()) {
            OrganizationUnit organizationUnit = allOus.next();
            this.syncOrgUnit(organizationUnit, syncListener, requestContext);
        }
    }

    public List<Event> getByTrackedEntityInstanceId(String tei) {
        return Event.find(Event.class, "TRACKED_ENTITY_INSTANCE = ?", tei);
    }

    public Event getRegistrationEvent(String tei) throws NotFoundInDBException {
        List<Event> events = Event.find(Event.class, "TRACKED_ENTITY_INSTANCE = ?", tei);
        if (events == null || events.isEmpty()) {
            throw new NotFoundInDBException("Registration event not found");
        } else {
            return events.get(0);
        }
    }

    private String getLastSyncKey(OrganizationUnit ou) {
        return SETTING_EVENTS_LAST_SYNCED_PREFIX + ou.getOuId();
    }

    private String getLastSyncedForOu(OrganizationUnit ou) {
        Setting setting = settingService.get(getLastSyncKey(ou));
        if (setting == null) {
            return "1992-08-16";
        } else {
            try {
                return DateUtils.dateTimeStringToDateOnlyString(setting.getValue());
            } catch (ParseException e) {
                return "1992-08-16";
            }
        }
    }

    private void syncOrgUnit(final OrganizationUnit ou, final SyncListener syncListener, final RequestContext requestContext) {
        syncPageFromServer(1, ou, syncListener, requestContext);
    }

    private void syncPageFromServer(final int page, final OrganizationUnit ou,
                                    final SyncListener syncListener, final RequestContext requestContext) {
        syncListener.onProgress(String.format("Starting to synchronize set %d of events of %s", page, ou.getDisplayName()));
        requestContext.sent();
        HashMap<String, String> params = new HashMap<>();
        params.put(KEY_OU, ou.getOuId());
        params.put(KEY_LAST_UPDATED, getLastSyncedForOu(ou));
        params.put(PAGE_PARAM, Integer.toString(page));
        GsonRequest<EventResponse> gsonRequest = new GsonRequest<>(
                getSyncEndpointUrl(params), EventResponse.class, headers, new Response.Listener<EventResponse>() {
            @Override
            public void onResponse(final EventResponse response) {
                Log.d("EVENT_SYNC", "Response received for syncing events of " + ou.getDisplayName() + " page " + page);
                syncListener.onProgress(String.format("Data received for set %d of events of %s", page, ou.getDisplayName()));

                Pager pager = response.getPager();
                if (pager.getPage() == 1) {
                    for (int i = 1; i < pager.getPageCount(); i++) {
                        syncPageFromServer(i + 1, ou, syncListener, requestContext);
                    }
                }

                SugarTransactionHelper.doInTransaction(new SugarTransactionHelper.Callback() {
                    @Override
                    public void manipulateInTransaction() {
                        syncListener.onProgress(String.format("Persisting set %d of events of %s", page, ou.getDisplayName()));
                        List<Event> events = response.getEvents();
                        for (Event event : events) {
                            event.saveToDb(true);
                        }
                        syncListener.onProgress(String.format("Persisted set %d of events of %s", page, ou.getDisplayName()));
                        requestContext.recieved();
                        if (requestContext.isDone()) {
                            settingService.put(getLastSyncKey(ou), DateUtils.now());
                            syncListener.onDone(null);
                        }
                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("EVENT_SYNC", "Error in syncing events of " + ou.getDisplayName());
                syncListener.onError(error);
            }
        });
        Log.d("EVENT_SYNC", "Started syncing events of " + ou.getDisplayName() + " page " + page);
        fireRequest(gsonRequest);
    }

    @Override
    public String getPostEndpointUrl() {
        return this.baseUrl + "events.json?strategy=CREATE_AND_UPDATE";
    }

    @Override
    public String getDeleteEndpointUrl(String id) {
        return this.baseUrl + "events/" + id;
    }

    @Override
    public String getSyncEndpointQueryString(Map<String, String> params) {
        String orgUnit = params.get(KEY_OU);
        String lastUpdated = params.get(KEY_LAST_UPDATED);
        StringBuilder urlBuilder = new StringBuilder("events.json?orgUnit=");
        urlBuilder.append(orgUnit);
        urlBuilder.append("&lastUpdated=");
        urlBuilder.append(lastUpdated);
        if (params.containsKey(PAGE_PARAM)) {
            urlBuilder.append("&page=");
            urlBuilder.append(params.get(PAGE_PARAM));
        }
        urlBuilder.append("&fields=event,trackedEntityInstance,programStage,orgUnit,eventDate,lastUpdated,coordinate,dataValues[lastUpdated,dataElement,value]&totalPages=true&pageSize=20");
        return urlBuilder.toString();
    }
}
