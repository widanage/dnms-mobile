package com.cwidanage.dnms.services;

/**
 * @author Chathura Widanage
 */

public interface SyncListener {

    void onDone(String msg);

    void onProgress(String msg);

    void onError(Exception e);
}
