package com.cwidanage.dnms.listeners;

import com.cwidanage.dnms.entity.Event;

public interface EventEditRequestListener {

    void onEditEventRequest(Event event, int position);
}
