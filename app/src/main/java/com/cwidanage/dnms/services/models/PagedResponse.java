package com.cwidanage.dnms.services.models;

/**
 * @author Chathura Widanage
 */

public class PagedResponse {

    protected Pager pager;

    public Pager getPager() {
        return pager;
    }

    public void setPager(Pager pager) {
        this.pager = pager;
    }
}
