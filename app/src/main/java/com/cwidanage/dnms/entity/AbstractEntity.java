package com.cwidanage.dnms.entity;

import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.exceptions.validation.ValidationFailedException;
import com.cwidanage.dnms.util.JsonIgnore;
import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

/**
 * @author Chathura Widanage
 */

public abstract class AbstractEntity extends SugarRecord {

    @JsonIgnore
    @Expose
    private boolean fresh;

    @JsonIgnore
    @Expose
    private boolean shouldSync;

    @JsonIgnore
    @Expose
    private boolean markForDelete;

    @JsonIgnore
    @Expose
    private boolean temporary;

    @JsonIgnore
    @Expose
    @Ignore
    private boolean relationsLoaded;

    public boolean isMarkForDelete() {
        return markForDelete;
    }

    public void setMarkForDelete(boolean markForDelete) {
        this.markForDelete = markForDelete;
    }

    public boolean isTemporary() {
        return temporary;
    }

    public void setTemporary(boolean temporary) {
        this.temporary = temporary;
    }

    public void loadRelations() throws NotFoundInDBException {
    }

    public boolean isFresh() {
        return fresh;
    }

    public void setFresh(boolean fresh) {
        this.fresh = fresh;
    }

    public boolean isShouldSync() {
        return shouldSync;
    }

    public void setShouldSync(boolean shouldSync) {
        this.shouldSync = shouldSync;
    }

    public void setRelationsLoaded(boolean relationsLoaded) {
        this.relationsLoaded = relationsLoaded;
    }

    public abstract void saveToDb(boolean noValidation) throws ValidationFailedException;

    public void saveToDb() throws ValidationFailedException {
        this.saveToDb(false);
    }

    public void setSynced() {
        this.fresh = false;
        this.shouldSync = false;
        this.temporary = false;
    }

    public void setRelationsLoaded() {
        this.relationsLoaded = true;
    }

    public boolean isRelationsLoaded() {
        return relationsLoaded;
    }
}
