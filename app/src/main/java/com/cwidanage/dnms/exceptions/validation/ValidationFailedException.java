package com.cwidanage.dnms.exceptions.validation;

/**
 * @author Chathura Widanage
 */

public class ValidationFailedException extends Exception {

    private int stringId;

    public ValidationFailedException(String msg) {
        super(msg);
    }

    public ValidationFailedException(int stringId) {
        this.stringId = stringId;
    }

    public int getStringId() {
        return stringId;
    }

    public void setStringId(int stringId) {
        this.stringId = stringId;
    }
}
