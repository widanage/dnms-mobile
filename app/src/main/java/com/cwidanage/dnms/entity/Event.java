package com.cwidanage.dnms.entity;

import com.cwidanage.dnms.exceptions.NotFoundInDBException;
import com.cwidanage.dnms.services.InstanceSpecificIDs;
import com.cwidanage.dnms.services.TrackedEntityInstanceService;
import com.cwidanage.dnms.util.JsonIgnore;
import com.google.gson.annotations.Expose;
import com.orm.dsl.Ignore;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Chathura Widanage
 */
@Table
public class Event extends AbstractEntity {

    @Unique
    private String event;
    private String programStage;
    private String trackedEntityInstance;
    private Date eventDate;

    @JsonIgnore
    private Date lastUpdated;

    private String orgUnit;

    @Ignore
    private String program = InstanceSpecificIDs.PROGRAM;

    @Ignore
    private List<DataValue> dataValues = new ArrayList<>();

    @Ignore
    private Coordinate coordinate;

    @JsonIgnore
    @Expose
    @Ignore
    private TrackedEntityInstance teiBean;

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(String orgUnit) {
        this.orgUnit = orgUnit;
    }

    public List<DataValue> getDataValues() {
        try {
            this.loadRelations();
        } catch (NotFoundInDBException e) {
            //do nothing
        }
        return dataValues;
    }

    public void setDataValues(List<DataValue> dataValues) {
        this.dataValues = dataValues;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getProgramStage() {
        return programStage;
    }

    public void setProgramStage(String programStage) {
        this.programStage = programStage;
    }

    public String getTrackedEntityInstance() {
        return trackedEntityInstance;
    }

    public void setTrackedEntityInstance(String trackedEntityInstance) {
        this.trackedEntityInstance = trackedEntityInstance;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public TrackedEntityInstance getTeiBean() {
        return teiBean;
    }

    public void setTeiBean(TrackedEntityInstance teiBean) {
        this.teiBean = teiBean;
    }

    @Override
    public void loadRelations() throws NotFoundInDBException {
        if (this.isRelationsLoaded()) {
            return;
        }
        List<Coordinate> coordinates = Coordinate.find(Coordinate.class, "EVENT_ID = ?", this.getEvent());
        if (!coordinates.isEmpty()) {
            this.coordinate = coordinates.get(0);
        }
        this.dataValues = DataValue.find(DataValue.class, "EVENT_ID = ?", this.getEvent());
        this.setRelationsLoaded();
    }

    @Override
    public void setMarkForDelete(boolean markForDelete) {
        if (this.isFresh()) {
            for (DataValue dataValue : this.getDataValues()) {
                dataValue.delete();
            }
            super.delete();
        } else {
            super.setMarkForDelete(markForDelete);
            Event.update(this);
        }
    }

    @Override
    public boolean delete() {
        for (DataValue dataValue : DataValue.find(DataValue.class, "EVENT_ID = ?", this.getEvent())) {
            dataValue.delete();
        }
        return super.delete();
    }

    @Override
    public void saveToDb(boolean noValidation) {

        if (this.coordinate != null) {
            this.coordinate.setEventId(this.getEvent());
            this.coordinate.saveToDb(false);
        }

        if (this.dataValues != null) {
            for (DataValue dataValue : this.dataValues) {
                if (dataValue.getValue() == null) {
                    continue;
                }
                dataValue.setEventId(this.getEvent());
                dataValue.generateUniqueId();

                if (dataValue.isShouldSync()) {
                    this.setShouldSync(true);
                }

                DataValue.update(dataValue);
            }
        }

        if (this.orgUnit == null) {
            try {
                TrackedEntityInstance byId = TrackedEntityInstanceService.INSTANCE.getById(this.trackedEntityInstance);
                this.setOrgUnit(byId.getOrgUnit());
            } catch (NotFoundInDBException e) {
                //
            }
        }
        Event.update(this);
    }

    @Override
    public void setSynced() {
        super.setSynced();
        if (this.coordinate != null) {
            this.coordinate.setSynced();
        }

        if (this.dataValues != null) {
            for (DataValue dataValue : this.dataValues) {
                dataValue.setSynced();
            }
        }

        Event.update(this);
    }
}
