package com.cwidanage.dnms.entity;

import com.cwidanage.dnms.exceptions.validation.ValidationFailedException;
import com.orm.SugarRecord;
import com.orm.dsl.Unique;

/**
 * @author Chathura Widanage
 */

public class Code extends AbstractEntity {

    @Unique
    private String code;

    private boolean used;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    @Override
    public void saveToDb(boolean noValidation) throws ValidationFailedException {
        SugarRecord.save(this);
    }
}
